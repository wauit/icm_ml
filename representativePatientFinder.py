# -*- coding: utf-8 -*-
"""
Created on Mon Jul  9 13:04:11 2018

@author: Ifrah
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os

from params import batch_size
from ggplot import *
from sklearn.manifold import TSNE
from sklearn.cluster import DBSCAN

from vae_mergedAE import *
from keras.models import Model

import matplotlib.pyplot as plt
import matplotlib.cm as cm
from mpl_toolkits.axes_grid1 import make_axes_locatable

from sklearn.preprocessing import StandardScaler
from sklearn.cluster import AffinityPropagation
from sklearn import metrics
from sklearn.datasets.samples_generator import make_blobs

import string
#%%
def plot_tSNE(model,
                 data,
                 perplexity = 30,
                 batch_size=batch_size,
                 model_name="vae_CNA"):
    """Plots labels and MNIST digits as function of 2-dim latent vector
    # Arguments:
        models (tuple): encoder and decoder models
        data (tuple): test data and label
        batch_size (int): prediction batch size
        model_name (string): which model is using this function
    """
    encoder = model
    x_dev = data
    
    z = encoder.predict(x_dev,verbose=1,batch_size=batch_size)

    tsne = TSNE(n_components=2,perplexity = 15)
    X_tsne = tsne.fit_transform(z)
    #X = X_tsne
    X = StandardScaler().fit_transform(X_tsne) 
    fig = plt.figure(figsize=(12, 10)) 
    ax = fig.add_subplot(111)
    m = cm.ScalarMappable(cmap=cm.jet)
    m.set_array(y_dev_cna)
    ax.scatter(X[:,0],X[:,1], c=y_dev_cna.ravel(),cmap=cm.jet)
    plt.colorbar(m)
    plt.show() 
    
    return(X)

#%%take dev_list and run into encoder. plot latent space using tSNE
###
tsne_pts=plot_tSNE(model=merged_encoder, data=dev_list)

#%%on latent space tSNE projection, use DBSCAN to identify clusters
###
db = DBSCAN(eps=0.3, min_samples=20).fit(tsne_pts)
core_samples = db.core_sample_indices_
labels = db.labels_
n_clusters = len(set(labels)) - (1 if -1 in labels else 0)
print(n_clusters)
#%%study clusters
###
clusters = [tsne_pts[labels == i] for i in range(n_clusters)]
clusters[0].shape
clusters[1].shape
clusters[2].shape
outliers = tsne_pts[labels == -1]
#%%clusters mapped to patients
###
patientID = np.array(list(range(1000)))
cl_patientID = [patientID[labels == i] for i in range(n_clusters)]

#%%cna and dna groups clusters
###
cl_cna = [dev_list[0][cl_patientID[i]] for i in range(n_clusters)]
cl_dna = [dev_list[1][cl_patientID[i]] for i in range(n_clusters)]



#%%
###
# #############################################################################
# Generate sample data
X=tsne_pts
labels_true = y_dev_cna.reshape(1000,)
# #############################################################################
# Compute Affinity Propagation
af = AffinityPropagation().fit(X)
cluster_centers_indices = af.cluster_centers_indices_
labels = af.labels_

n_clusters_ = len(cluster_centers_indices)

print('Estimated number of clusters: %d' % n_clusters_)
print("Homogeneity: %0.3f" % metrics.homogeneity_score(labels_true, labels))
print("Completeness: %0.3f" % metrics.completeness_score(labels_true, labels))
print("V-measure: %0.3f" % metrics.v_measure_score(labels_true, labels))
print("Adjusted Rand Index: %0.3f"
      % metrics.adjusted_rand_score(labels_true, labels))
print("Adjusted Mutual Information: %0.3f"
      % metrics.adjusted_mutual_info_score(labels_true, labels))
print("Silhouette Coefficient: %0.3f"
      % metrics.silhouette_score(X, labels, metric='sqeuclidean'))

# #############################################################################
# Plot result
import matplotlib.pyplot as plt
from itertools import cycle

plt.close('all')
plt.figure(1)
plt.clf()

colors = cycle('bgrcmykbgrcmykbgrcmykbgrcmyk')
ClustCenter = [X[cluster_centers_indices[k]] for k in range(n_clusters_)]

result = []
for i in range(n_clusters):
    temp=[a for a in cl_patientID[i] if a in cluster_centers_indices]
    result.append(temp)

cnaClust = [dev_list[0][result[i]] for i in range(n_clusters)]
dnaClust = [dev_list[1][result[i]] for i in range(n_clusters)]

for k, col in zip(range(n_clusters_), colors):
    class_members = labels == k
    cluster_center = X[cluster_centers_indices[k]]
    plt.plot(X[class_members, 0], X[class_members, 1], col + '.')
    plt.plot(cluster_center[0], cluster_center[1], 'o', markerfacecolor=col,
             markeredgecolor='k', markersize=14)
    for x in X[class_members]:
        plt.plot([cluster_center[0], x[0]], [cluster_center[1], x[1]], col)

plt.title('Estimated number of clusters: %d' % n_clusters_)
plt.show()

#%%plot histograms of cna and dna clusters
###
for i in range(3):
    x_df = pd.DataFrame(cnaClust[i],columns=list(string.ascii_uppercase[0:10]))
    x_bp = x_df.plot.box(return_type='both', patch_artist=True)

for i in range(3):
    x_df = pd.DataFrame(dnaClust[i],columns=list(string.ascii_uppercase[0:10]))
    x_bp = x_df.plot.box(return_type='both', patch_artist=True)
#%%dna up and down genes
geneNames = np.tile(list(string.ascii_uppercase[0:10]),(dnaClust[0].shape[0],1))
dna_upgenes = [[None] * dnaClust[i].shape[0] for i in range(n_clusters)]
dna_downgenes = [[None] * dnaClust[i].shape[0] for i in range(n_clusters)]

for j in range(n_clusters):
    for i in range(dnaClust[j].shape[0]):
        dna_upgenes[j][i]=geneNames[i][dnaClust[j][i]>=0.6]

for j in range(n_clusters):
    for i in range(dnaClust[j].shape[0]):
        dna_downgenes[j][i]=geneNames[i][dnaClust[j][i]<=0.4]
#%%cna up and down genes
geneNames = np.tile(list(string.ascii_uppercase[0:10]),(cnaClust[0].shape[0],1))
cna_upgenes = [[None] * cnaClust[i].shape[0] for i in range(n_clusters)]
cna_downgenes = [[None] * cnaClust[i].shape[0] for i in range(n_clusters)]

for j in range(n_clusters):
    for i in range(cnaClust[j].shape[0]):
        cna_upgenes[j][i]=geneNames[i][cnaClust[j][i]>=0.6]

for j in range(n_clusters):
    for i in range(cnaClust[j].shape[0]):
        cna_downgenes[j][i]=geneNames[i][cnaClust[j][i]<=0.4]
#%%z-space groups from clusters
###
zspace = [z[cl_patientID[i]] for i in range(n_clusters)]

#plot distribution of zspace
for i in range(n_clusters):
    x_df = pd.DataFrame(zspace[i])
    x_bp = x_df.plot.box(return_type='both', patch_artist=True)

#then find mean of original zspace
z_center=[np.mean(zspace[i], axis=0) for i in range(n_clusters)]
#%%take latent space cluster means to run through generator, obtain potential
###representative patients
from sklearn.preprocessing import MinMaxScaler 
z_center = np.asarray(z_center)
#z_scalar = MinMaxScaler()
#z_center_scaled = z_scalar.fit_transform(z_center)
rep_patient=generator.predict(z_center)
#rep_patient=np.asarray(rep_patient)
#%%
rep_patient[0][0]

#%%

