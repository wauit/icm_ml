# -*- coding: utf-8 -*-
"""
Created on Wed Jun 13 19:43:55 2018

@author: Ifrah
"""
from class_cna import CNA
from class_dnaEXPR import dnaEXPR

#%% CNA TRAIN SET
cna_train = CNA()
for i in range(200):
    cna_train.addPatient(10)
    cna_train.addPatient(15)
    cna_train.addPatient(16)
    cna_train.addPatient(29)
    cna_train.addPatient(25)
    cna_train.addPatient(35)
    cna_train.addPatient(25)
    cna_train.addPatient(45)
    cna_train.addPatient(48)
    cna_train.addPatient(50)
    
#%% CNA TEST SET
cna_dev=CNA()
for i in range(100):
    cna_dev.addPatient(10)
    cna_dev.addPatient(15)
    cna_dev.addPatient(16)
    cna_dev.addPatient(29)
    cna_dev.addPatient(25)
    cna_dev.addPatient(35)
    cna_dev.addPatient(25)
    cna_dev.addPatient(45)
    cna_dev.addPatient(48)
    cna_dev.addPatient(50)

#%% DNA TEST SET
dna_train = dnaEXPR()
for i in range(200):
    dna_train.addPatient(10)
    dna_train.addPatient(15)
    dna_train.addPatient(16)
    dna_train.addPatient(29)
    dna_train.addPatient(25)
    dna_train.addPatient(35)
    dna_train.addPatient(25)
    dna_train.addPatient(45)
    dna_train.addPatient(48)
    dna_train.addPatient(50)
    
#%% CNA TEST SET
dna_dev=dnaEXPR()
for i in range(100):
    dna_dev.addPatient(10)
    dna_dev.addPatient(15)
    dna_dev.addPatient(16)
    dna_dev.addPatient(29)
    dna_dev.addPatient(25)
    dna_dev.addPatient(35)
    dna_dev.addPatient(25)
    dna_dev.addPatient(45)
    dna_dev.addPatient(48)
    dna_dev.addPatient(50)
#%%    
#showPatients(train.data[1:10,1:10])    
#train.data
#train.labels  