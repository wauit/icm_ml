# -*- coding: utf-8 -*-
"""
Created on Wed Jun 13 20:31:16 2018

@author: Ifrah
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from keras.layers import Lambda, Input, Dense, BatchNormalization
from keras.callbacks import TensorBoard
from keras.models import Model
from keras.utils import plot_model

from params import input_shape, intermediate_dim, latent_dim, original_dim
from params import y_dev_dna, epochs
from params import x_train_dna, y_train_dna, x_dev_dna, batch_size
from vae_functions import sampling, vae_objective

from plotting import plot_tSNE

import tensorflow as tf
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os


#%%

PATH = os.getcwd()
NET_DIR = PATH + "\\vae\\dnaEXPR\\toy"
LOG_DIR = NET_DIR + '\\tflogs'

#%%
patientkey=pd.Series(list(range(0, 1000)))
severity = pd.Series([int(i) for i in y_dev_dna.ravel()])
df_ = pd.DataFrame({"patient": patientkey,"severity":severity})
metadata = os.path.join(LOG_DIR, 'metadata.tsv')
#add as many labels to the embedding as you would like.
#this is a label used to identify key points about the latent space
#that is being used to describe the patient
df_.to_csv(metadata, sep='\t',encoding='utf-8', index = False)

weights_file = os.path.join(LOG_DIR, 
               "_vae_dna_%d_latent_%d_epochs.hdf5") % (latent_dim,epochs)
#%%
# VAE model = encoder + decoder

#%% build encoder 
inputs = Input(shape=input_shape, name='encoder_input')

#BatchNormalization
#GaussianNoise

encoder_h = Dense(intermediate_dim, activation='relu')(inputs)
#this has no activation. it just takes h, does nothing to it
#and the output space has a dimensionality of latent_dim

#feed same inputs to these nodes. these will later be used to calculate 
#the z
z_mean = Dense(latent_dim, name='z_mean')(encoder_h)
z_log_var = Dense(latent_dim, name='z_log_var')(encoder_h)

# use reparameterization trick to push the sampling out as input
# note that "output_shape" isn't necessary with the TensorFlow backend
z = Lambda(sampling, name='z')([z_mean, z_log_var])

#%% build decoder 

decoder_h1 = Dense(intermediate_dim, activation='relu')
decoder_h2 = Dense(original_dim, activation='sigmoid',name='decoder_h2')


z_decoded = decoder_h1(z)
x_decoded = decoder_h2(z_decoded)

#%% instantiate VAE model

vae = Model(inputs=inputs, outputs=x_decoded, name='vae_mlp')
plot_model(vae,to_file=os.path.join(NET_DIR,'vae_dna.png'), 
           show_shapes=True)

#%%
merged_encoder = Model(inputs=inputs, outputs=z)

#%%generator
z_input = Input(shape=(latent_dim,))

decoder_h1 = Dense(intermediate_dim, activation='relu')
decoder_h2 = Dense(original_dim, activation='sigmoid',name='decoder_dna_h2')

z_decoded = decoder_h1(z_input)
x_decoded = decoder_h2(z_decoded)

generator = Model(inputs=z_input, outputs=x_decoded)

#%% compile VAE model

vae_loss=vae_objective(z_mean, z_log_var)
vae.compile(optimizer="adam", loss=vae_loss)

#%%
embed_freq=1
tb_callback=TensorBoard(log_dir=LOG_DIR, 
                        batch_size=batch_size,
                        #save layer output at x epoch 
                        embeddings_freq=embed_freq,
                        embeddings_metadata='metadata.tsv',
                        embeddings_data=x_dev_dna,
                        embeddings_layer_names=['z'],
                        histogram_freq=1,
                        write_grads=True)

#%%

from keras.callbacks import History
hist_cb = History()
vae.fit(x_train_dna, x_train_dna, shuffle=True, epochs=epochs, batch_size=batch_size,
        callbacks=[hist_cb], validation_data=(x_dev_dna, x_dev_dna))
vae.save_weights(weights_file)

#%%
#import json
#import matplotlib.cm as cm
#from sklearn.preprocessing import StandardScaler
#from pylab import *
##text=open("state.txt","rb").read()
#
#json_data=open("state.txt")
#data = json.load(json_data)
#
#tsne_x=[]
#tsne_y=[]
#for x in list(range(1000)):
#    tsne_x.append(data[0]['projections'][x]['tsne-0'])
#    tsne_y.append(data[0]['projections'][x]['tsne-1'])
#    
#tsne = np.c_[tsne_x,tsne_y]
#X = StandardScaler().fit_transform(tsne) 
##X=tsne
#fig = plt.figure(figsize=(12, 10)) 
#ax = fig.add_subplot(111)
#
#m = cm.ScalarMappable(cmap=cm.jet)
#m.set_array(y_dev)
#ax.scatter(X[:,0],X[:,1], c=y_dev.ravel(),cmap=cm.jet)
#plt.colorbar(m)
#plt.show() 
#
#json_data.close()
#
#from sklearn.cluster import DBSCAN
#db = DBSCAN(eps=0.3, min_samples=20).fit(X)
#core_samples = db.core_sample_indices_
#labels = db.labels_
#n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
#print(n_clusters_)
#
#clusters = [X[labels == i] for i in range(n_clusters_)]
#clusters[0].shape
#clusters[1].shape
#outliers = X[labels == -1]

#%%
#import json
#import matplotlib.cm as cm
#from sklearn.preprocessing import StandardScaler
#from pylab import *
##text=open("state.txt","rb").read()
#
#json_data=open("state.txt")
#data = json.load(json_data)
#
#tsne_x=[]
#tsne_y=[]
#for x in list(range(1000)):
#    tsne_x.append(data[0]['projections'][x]['tsne-0'])
#    tsne_y.append(data[0]['projections'][x]['tsne-1'])
#    
#tsne = np.c_[tsne_x,tsne_y]
#X = StandardScaler().fit_transform(tsne) 
#
#fig = plt.figure(figsize=(12, 10)) 
#ax = fig.add_subplot(111)
#
#m = cm.ScalarMappable(cmap=cm.jet)
#m.set_array(y_dev)
#ax.scatter(X[:,0],X[:,1], c=y_dev.ravel(),cmap=cm.jet)
#plt.colorbar(m)
#plt.show() 
#
#json_data.close()
#
#from sklearn.cluster import DBSCAN
#db = DBSCAN(eps=0.3, min_samples=20).fit(X)
#core_samples = db.core_sample_indices_
#labels = db.labels_
#n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
#print(n_clusters_)
#
#clusters = [X[labels == i] for i in range(n_clusters_)]
#clusters[0].shape
#clusters[1].shape
#outliers = X[labels == -1]
#%%
#import numpy as np
#import matplotlib.pyplot as plt
#
#data = X
#labels = ['point{0}'.format(i) for i in range(X.shape[0])]
#
#plt.subplots_adjust(bottom = 0.1)
#plt.scatter(X[:,0],X[:,1], c=y_dev.ravel(),cmap=cm.jet)
#
#for label, x, y in zip(labels, data[:, 0], data[:, 1]):
#    plt.annotate(
#        label,
#        xy=(x, y), xytext=(-20, 20),
#        textcoords='offset points', ha='right', va='bottom',
#        bbox=dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.5),
#        arrowprops=dict(arrowstyle = '->', connectionstyle='arc3,rad=0'))
#
#plt.show()

#%%
#plt.subplots_adjust(bottom = 0.1)
#plt.scatter(clusters[0][:,0],clusters[0][:,1])
#plt.show()
#
#plt.subplots_adjust(bottom = 0.1)
#plt.scatter(clusters[0][:,0],clusters[0][:,1])
#plt.show()

#%%
#import seaborn as sns
#def _plot_scatter(output_res, num_clusters, color_palette, alpha=0.5, symbol='o'):
#    for ci in range(num_clusters):
#        cur_plot_rows = ci
#        cur_color = color_palette[ci]
#        plt.plot(output_res[cur_plot_rows, 0], output_res[cur_plot_rows, 1], symbol, 
#        color=cur_color, label=ci, alpha=alpha)
#        
#
#def _plot_kde(output_res, num_clusters, color_palette, alpha=0.5):
#    for ci in range(num_clusters):
#        cur_plot_rows = ci
#        cur_cmap = sns.light_palette(color_palette[ci], as_cmap=True)
#        sns.kdeplot(output_res[cur_plot_rows, 0], output_res[cur_plot_rows, 1], cmap=cur_cmap, shade=True, alpha=alpha,
#            shade_lowest=False)
#        centroid = output_res[cur_plot_rows, :].mean(axis=0)
#        plt.annotate('%s' % ci, xy=centroid, xycoords='data', alpha=0.5,
#                     horizontalalignment='center', verticalalignment='center')
#%%

#from tsne import *
#
#num_outputs = 2
#perplexity = 30
#ptSNE = Parametric_tSNE(high_dims, num_outputs, perplexity)
#output_res = ptSNE.transform(train_data)
#do_pretrain = True
#
#encoder = Model(inputs=vae.input, outputs=z_mean)
#z_train = encoder.predict(x_train,verbose=1,batch_size=batch_size)
#train_data = z_train
#high_dims = train_data.shape[1]
#
#from tensorflow.contrib.keras import layers
#all_layers = [layers.Dense(10, input_shape=(high_dims,), activation='sigmoid', kernel_initializer='glorot_uniform'),
#layers.Dense(100, activation='sigmoid', kernel_initializer='glorot_uniform'),
#layers.Dense(num_outputs, activation='relu', kernel_initializer='glorot_uniform')]
#ptSNE = Parametric_tSNE(high_dims, num_outputs, perplexity, all_layers=all_layers)
#
#z_train = encoder.predict(x_train,verbose=1,batch_size=batch_size)
#test_data = z_train
#test_res = ptSNE.transform(test_data)
#
#    plt.figure()
#    num_clusters = 14
#    color_palette = sns.color_palette("hls", num_clusters)
#    sns.kdeplot(output_res[, 0], output_res[, 1], cmap=cur_cmap, shade=True, alpha=alpha,
#            shade_lowest=False)
#    # Create a contour plot of training data
#    _plot_kde(output_res, color_palette, 0.5)
#    
#    # Scatter plot of test data
#    _plot_scatter(test_res, color_palette, alpha=0.1, symbol='*')
#    
#    leg = plt.legend(bbox_to_anchor=(1.0, 1.0))
#    # Set marker to be fully opaque in legend
#    for lh in leg.legendHandles: 
#        lh._legmarker.set_alpha(1.0)
#
#    plt.title('{label:s} Transform with {num_clusters:d} clusters\n{test_data_tag:s} Data'.format(label=label, num_clusters=num_clusters, test_data_tag=test_data_tag.capitalize()))
#
#plt.show()

#%%
#
#
#models = (encoder, generator)
#data = (x_dev, y_dev)
#
##plot_first2D(models,
##             data,
##             batch_size=batch_size,
##             model_name="vae_first2D")
#    
#import matplotlib.pyplot as plt
#import matplotlib.cm as cm
#from sklearn.manifold import TSNE 
#
#z = encoder.predict(x_dev,verbose=1,batch_size=batch_size)
#
#model = TSNE(learning_rate=1, early_exaggeration=4, n_iter=6000, n_components=5, method='exact')
#transformed = model.fit_transform(z)
#xs = transformed[:,0]
#ys = transformed[:,1]
#    
#fig = plt.figure(figsize=(12, 10)) 
#ax = fig.add_subplot(111)
#
#m = cm.ScalarMappable(cmap=cm.jet)
#m.set_array(y_dev)
#ax.scatter(xs, ys, c=y_dev.ravel(),cmap=cm.jet)
#plt.colorbar(m)
#plt.show() 
    

    #%%
#encoder = Model(input=vae.input, output=vae.get_layer(index=-5).output)
#
#decoder_input = Input(shape=(latent_dim,))
#h_decoded = decoder_h1(decoder_input)
#x_decoded = decoder_h2(h_decoded)
#generator = Model(input=decoder_input, output=x_decoded)

#%%
#step = tf.placeholder(tf.int32)
#batch_id = tf.placeholder(tf.int32)
#embeddings_data=x_dev
#assign_embeddings = []
#embeddings_vars = {}
#
#embedding_input = vae.get_layer("z").output
#embedding_size = np.prod(embedding_input.shape[1:])
#embedding_input = tf.reshape(embedding_input,(step, int(embedding_size)))
#shape = (embeddings_data[0].shape[0], int(embedding_size))
#embedding = tf.Variable(tf.zeros(shape),name="z" + '_embedding')
#embeddings_vars["z"] = embedding
#batch = tf.assign(embedding[batch_id:batch_id + step],embedding_input)
#assign_embeddings.append(batch)
#
#saver=tf.train.Saver(list(embeddings_vars.values()))
#saver.save(sess, os.path.)
#%%
#tensorboard --host localhost --logdir=E:\\Workspace\\MPhil_workspace\\ICM_ML\\vae\\dnaEXPR\\toy\\tflogs


