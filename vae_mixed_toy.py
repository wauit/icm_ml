# -*- coding: utf-8 -*-
"""
Created on Thu Jun 28 13:12:30 2018

@author: Ifrah
"""
#%%
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from keras.layers import Lambda, Input, Dense, BatchNormalization
from keras.callbacks import TensorBoard
from keras.models import Model
from keras.utils import plot_model

from params import input_shape, intermediate_dim, latent_dim, original_dim
from params import y_dev, epochs
from params import x_train, y_train, x_dev, batch_size
from vae_functions import sampling, vae_objective

from plotting import plot_tSNE

import tensorflow as tf
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os

#%%
PATH = os.getcwd()
NET_DIR = PATH + "\\vae\\mixed\\toy"
LOG_DIR = NET_DIR + '\\tflogs'

#%%
patientkey=pd.Series(list(range(0, 1000)))
severity = pd.Series([int(i) for i in y_dev.ravel()])
df_ = pd.DataFrame({"patient": patientkey,"severity":severity})
metadata = os.path.join(LOG_DIR, 'metadata.tsv')
#add as many labels to the embedding as you would like.
#this is a label used to identify key points about the latent space
#that is being used to describe the patient
df_.to_csv(metadata, sep='\t',encoding='utf-8', index = False)

weights_file = os.path.join(LOG_DIR, 
               "_vae_mixed_%d_latent_%d_epochs.hdf5") % (latent_dim,epochs)

#%% build encoder 
dna_inputs = Input(shape=input_shape, name='encoder_input')
cna_inputs = Input(shape=input_shape, name='encoder_input')

encoder_dna = Dense(intermediate_dim, activation='relu')(dna_inputs)
encoder_cna = Dense(intermediate_dim, activation='relu')(cna_inputs)
#this has no activation. it just takes h, does nothing to it
#and the output space has a dimensionality of latent_dim

#feed same inputs to these nodes. these will later be used to calculate 
#the z
z_mean_dna = Dense(latent_dim, name='z_mean')(encoder_dna)
z_log_var_dna = Dense(latent_dim, name='z_log_var')(encoder_dna)

z_mean_cna = Dense(latent_dim, name='z_mean')(encoder_cna)
z_log_var_cna = Dense(latent_dim, name='z_log_var')(encoder_cna)

# use reparameterization trick to push the sampling out as input
# note that "output_shape" isn't necessary with the TensorFlow backend
z_dna = Lambda(sampling, name='z')([z_mean_dna, z_log_var_dna])
z_cna = Lambda(sampling, name='z')([z_mean_cna, z_log_var_cna])
#%% build decoder
decoder_h1 = Dense(intermediate_dim, activation='relu')
decoder_h2 = Dense(original_dim, activation='sigmoid',name='decoder_h2')


z_decoded_dna = decoder_h1(z_dna)
x_decoded_dna = decoder_h2(z_decoded_dna)


z_decoded_cna = decoder_h1(z_cna)
x_decoded_cna = decoder_h2(z_decoded_cna)
#%% instantiate VAE model
dna_vae=Model(inputs=dna_inputs, outputs=x_decoded_dna)
cna_vae=Model(inputs=cna_inputs, outputs=x_decoded_cna)

dna_vae_loss=vae_objective(z_mean_dna, z_log_var_dna)
dna_vae.compile(optimizer="adam", loss=dna_vae_loss)

cna_vae_loss=vae_objective(z_mean_cna, z_log_var_cna)
cna_vae.compile(optimizer="adam", loss=cna_vae_loss)
#%%
encoder_left = Dense(intermediate_dim, activation='relu')(z_mean_dna)
encoder_right = Dense(intermediate_dim, activation='relu')(z_mean_cna)

z_mean_left = Dense(latent_dim, name='z_mean')(encoder_left)
z_log_var_left = Dense(latent_dim, name='z_log_var')(encoder_left)

z_mean_right = Dense(latent_dim, name='z_mean')(encoder_right)
z_log_var_right = Dense(latent_dim, name='z_log_var')(encoder_right)

z_left = Lambda(sampling, name='z')([z_mean_left, z_log_var_left])
z_right = Lambda(sampling, name='z')([z_mean_right, z_log_var_right])

z_decoded_left = decoder_h1(z_left)
x_decoded_left = decoder_h2(z_decoded_left)

z_decoded_right = decoder_h1(z_right)
x_decoded_right = decoder_h2(z_decoded_right)

#%%
left_vae=Model(inputs=encoder_left, outputs=x_decoded_left)
right_vae=Model(inputs=encoder_right, outputs=x_decoded_right)

vae_loss_left=vae_objective(z_mean_left, z_log_var_left)
left_vae.compile(optimizer="adam", loss=vae_loss_left)

vae_loss_right=vae_objective(z_mean_right, z_log_var_right)
right_vae.compile(optimizer="adam", loss=vae_loss_right)


vae=Model(inputs=z_left, outputs=z_right)

plot_model(vae,to_file=os.path.join(NET_DIR,'vae_mixed_network.png'), 
           show_shapes=True)

#%% compile VAE model
vae_loss_middle=vae_objective(z_mean_right, z_log_var_right)
vae.compile(optimizer="adam", loss=vae_loss_middle)

#%%
from keras.callbacks import History
hist_cb = History()
vae.fit(x_train, x_train, shuffle=True, epochs=epochs, batch_size=batch_size,
        callbacks=[hist_cb], validation_data=(x_dev, x_dev))
vae.save_weights(weights_file)

