# -*- coding: utf-8 -*-
"""
Created on Wed Jun 13 19:35:12 2018

@author: Ifrah
"""

import numpy as np
np.random.seed(42)

#%%
class CNA:
    "This class creates and modifies the CNA dataset"
    def __init__(self):
        self.geneNum = 10
        self.data = np.array([]).reshape(0,self.geneNum)
        self.label = np.array([]).reshape(0,1)
        self.severity = np.array([]).reshape(0,1)
        self.cnalist = [-2, -1, 0, 1, 2]
        #self.cnalist = [1, 2, 3, 4, 5]
        
    def __bioMarkers(self,os):
        if(os>=0 and os<20):
            FAB = np.random.choice(self.cnalist,3, 
                                   p=[0.0025,0.0025,0.005,0.495,0.495])
            DIE = np.random.choice(self.cnalist,3, 
                                   p=[0.495,0.495,0.005,0.0025,0.0025])
            CGHJ = np.random.choice(self.cnalist,4, 
                                   p=[0.2,0.2,0.2,0.2,0.2])
            severity = 0
            
        elif(os>=20 and os<40):
            FAB = np.random.choice(self.cnalist,3, 
                                   p=[0.00025,0.49725,0.005,0.49725,0.00025])
            DIE = np.random.choice(self.cnalist,3, 
                                   p=[0.0025,0.495,0.005,0.495,0.0025])
            CGHJ = np.random.choice(self.cnalist,4, 
                                   p=[0.2,0.2,0.2,0.2,0.2])
            severity = 1
            
        elif(os>=40):
            FAB = np.random.choice(self.cnalist,3, 
                                   p=[0.0025,0.495,0.005,0.495,0.0025])
            DIE = np.random.choice(self.cnalist,3, 
                                   p=[0.0025,0.0025,0.005,0.495,0.495])
            CGHJ = np.random.choice(self.cnalist,4, 
                                   p=[0.2,0.2,0.2,0.2,0.2])
            severity = 2
            
        CNApattern = np.array([FAB[1],FAB[2],CGHJ[0],
                               DIE[0],DIE[2],FAB[0],
                               CGHJ[1],CGHJ[2],DIE[1],CGHJ[3]]).reshape(1,10)
    
        return(CNApattern, severity) 
            
    def addPatient(self, os):
        d,s = self.__bioMarkers(os)
        self.data = np.vstack((self.data, d))
        self.labels = np.vstack((self.label, os))
        self.severity = np.vstack((self.severity,s))
        
        
#%%
class CNA:
    "This class creates and modifies the CNA dataset"
    def __init__(self):
        self.geneNum = 10
        self.data = np.array([]).reshape(0,self.geneNum)
        self.label = np.array([]).reshape(0,1)
        self.severity = np.array([]).reshape(0,1)
        self.cnalist = [-2, -1, 0, 1, 2]
        #self.cnalist = [1, 2, 3, 4, 5]
        
    def __bioMarkers(self,os):
        if(os>=0 and os<20):
            FAB = np.random.choice(self.cnalist,3, 
                                   p=[0.025,0.025,0.05,0.45,0.45])
            DIE = np.random.choice(self.cnalist,3, 
                                   p=[0.45,0.45,0.05,0.025,0.025])
            CGHJ = np.random.choice(self.cnalist,4, 
                                   p=[0.2375,0.2375,0.05,0.2375,0.2375])
            severity = 0
            
        elif(os>=20 and os<40):
            FAB = np.random.choice(self.cnalist,3, 
                                   p=[0.025,0.45,0.05,0.45,0.025])
            DIE = np.random.choice(self.cnalist,3, 
                                   p=[0.025,0.45,0.05,0.45,0.025])
            CGHJ = np.random.choice(self.cnalist,4, 
                                   p=[0.2375,0.2375,0.05,0.2375,0.2375])
            severity = 1
            
        elif(os>=40):
            FAB = np.random.choice(self.cnalist,3, 
                                   p=[0.025,0.45,0.05,0.45,0.025])
            DIE = np.random.choice(self.cnalist,3, 
                                   p=[0.025,0.025,0.05,0.45,0.45])
            CGHJ = np.random.choice(self.cnalist,4, 
                                   p=[0.2375,0.2375,0.05,0.2375,0.2375])
            severity = 2
            
        CNApattern = np.array([FAB[1],FAB[2],CGHJ[0],
                               DIE[0],DIE[2],FAB[0],
                               CGHJ[1],CGHJ[2],DIE[1],CGHJ[3]]).reshape(1,10)
    
        return(CNApattern, severity) 
            
    def addPatient(self, os):
        d,s = self.__bioMarkers(os)
        self.data = np.vstack((self.data, d))
        self.labels = np.vstack((self.label, os))
        self.severity = np.vstack((self.severity,s))