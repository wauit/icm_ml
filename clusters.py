# -*- coding: utf-8 -*-
"""
Created on Thu Jun 28 08:58:02 2018

@author: Ifrah
"""

import json
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from sklearn.preprocessing import StandardScaler
from pylab import *
import pandas as pd
from params import *

import string 
#text=open("state.txt","rb").read()

json_data=open(os.getcwd()+"\\vae\\concatenate\\z_concat.txt")
data = json.load(json_data)

tsne_x=[]
tsne_y=[]
for x in list(range(1000)):
    tsne_x.append(data[0]['projections'][x]['tsne-0'])
    tsne_y.append(data[0]['projections'][x]['tsne-1'])
    
tsne = np.c_[tsne_x,tsne_y]
X = StandardScaler().fit_transform(tsne) 

fig = plt.figure(figsize=(12, 10)) 
ax = fig.add_subplot(111)

m = cm.ScalarMappable(cmap=cm.jet)
m.set_array(y_dev)
ax.scatter(X[:,0],X[:,1], c=y_dev.ravel(),cmap=cm.jet)
plt.colorbar(m)
plt.show() 

json_data.close()

from sklearn.cluster import DBSCAN
db = DBSCAN(eps=0.3, min_samples=20).fit(X)
core_samples = db.core_sample_indices_
labels = db.labels_
n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
print(n_clusters_)

clusters = [X[labels == i] for i in range(n_clusters_)]
clusters[0].shape
clusters[1].shape
outliers = X[labels == -1]

#%%
groups = [x_dev_st[labels==i] for i in range(n_clusters_)]
groups[0]

#%%
group0_df = pd.DataFrame(groups[0],columns=list(string.ascii_uppercase[0:10]*2))
group0_bp = group0_df.plot.box(return_type='both', patch_artist=True)

#%%
group1_df = pd.DataFrame(groups[1],columns=list(string.ascii_uppercase[0:10]*2))
group1_bp = group1_df.plot.box(return_type='both', patch_artist=True)

#%%
group2_df = pd.DataFrame(groups[2],columns=list(string.ascii_uppercase[0:10]*2))
group2_bp = group1_df.plot.box(return_type='both', patch_artist=True)

#%%
group3_df = pd.DataFrame(groups[3],columns=list(string.ascii_uppercase[0:10]*2))
group3_bp = group1_df.plot.box(return_type='both', patch_artist=True)

#%%
#x_df_low = x_df[y_dev==0]
#x_bp_low = x_df_low.plot.box(return_type='both', patch_artist=True, color="red")
#x_df_low
#
#
#x_df_med = x_dfy_dev==1]
#x_bp_med = x_df_med.plot.box(return_type='both', patch_artist=True, color="gold")
#x_df_med
#
#x_df_high = x_df[x.severity==2]
#x_bp_high = x_df_high.plot.box(return_type='both', patch_artist=True, color="green")
#x_df_high

