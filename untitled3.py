# -*- coding: utf-8 -*-
"""
Created on Thu Jun 28 16:54:42 2018

@author: Ifrah
"""

#%%
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from keras.layers import Lambda, Input, Dense, BatchNormalization, concatenate, merge
from keras.callbacks import TensorBoard
from keras.models import Model
from keras.utils import plot_model

from params import input_shape, intermediate_dim, latent_dim, original_dim
from params import y_dev, epochs
from params import x_train, y_train, x_dev, batch_size
from vae_functions import sampling, vae_objective

from plotting import plot_tSNE

import tensorflow as tf
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os

#%%
PATH = os.getcwd()
NET_DIR = PATH + "\\vae\\shared\\toy"
LOG_DIR = NET_DIR + '\\tflogs'

#%%
patientkey=pd.Series(list(range(0, 1000)))
severity = pd.Series([int(i) for i in y_dev.ravel()])
df_ = pd.DataFrame({"patient": patientkey,"severity":severity})
metadata = os.path.join(LOG_DIR, 'metadata.tsv')
#add as many labels to the embedding as you would like.
#this is a label used to identify key points about the latent space
#that is being used to describe the patient
df_.to_csv(metadata, sep='\t',encoding='utf-8', index = False)

weights_file = os.path.join(LOG_DIR, 
               "_vae_dna_%d_latent_%d_epochs.hdf5") % (latent_dim,epochs)

#%% build encoder 
dna_inputs = Input(shape=input_shape, name='dna_input')
cna_inputs = Input(shape=input_shape, name='cna_input')

encoder_dna = Dense(intermediate_dim, activation='relu')(dna_inputs)
encoder_cna = Dense(intermediate_dim, activation='relu')(cna_inputs)
#this has no activation. it just takes h, does nothing to it
#and the output space has a dimensionality of latent_dim

#feed same inputs to these nodes. these will later be used to calculate 
#the z
z_mean_dna = Dense(latent_dim, name='z_mean_dna')(encoder_dna)
z_log_var_dna = Dense(latent_dim, name='z_log_var_dna')(encoder_dna)

z_mean_cna = Dense(latent_dim, name='z_mean_cna')(encoder_cna)
z_log_var_cna = Dense(latent_dim, name='z_log_var_cna')(encoder_cna)

# use reparameterization trick to push the sampling out as input
# note that "output_shape" isn't necessary with the TensorFlow backend
z_dna = Lambda(sampling, name='z_dna')([z_mean_dna, z_log_var_dna])
z_cna = Lambda(sampling, name='z_cna')([z_mean_cna, z_log_var_cna])

#%% build decoder
decoder_dna_h1 = Dense(intermediate_dim, activation='relu')
decoder_dna_h2 = Dense(original_dim, activation='sigmoid',name='decoder_dna_h2')


z_decoded_dna = decoder_dna_h1(z_dna)
x_decoded_dna = decoder_dna_h2(z_decoded_dna)

decoder_cna_h1 = Dense(intermediate_dim, activation='relu')
decoder_cna_h2 = Dense(original_dim, activation='sigmoid',name='decoder_cna_h2')

z_decoded_cna = decoder_cna_h1(z_cna)
x_decoded_cna = decoder_cna_h2(z_decoded_cna)

#%% instantiate VAE model
dna_vae=Model(inputs=dna_inputs, outputs=x_decoded_dna)
cna_vae=Model(inputs=cna_inputs, outputs=x_decoded_cna)

dna_vae_loss=vae_objective(z_mean_dna, z_log_var_dna)
dna_vae.compile(optimizer="adam", loss=dna_vae_loss)

cna_vae_loss=vae_objective(z_mean_cna, z_log_var_cna)
cna_vae.compile(optimizer="adam", loss=cna_vae_loss)


#%%
#merged_layer=merge([z_dna,z_cna], mode="concat", concat_axis=1)
merged_layer=concatenate([z_dna,z_cna])
#merged_layer=np.vstack([z_dna,z_cna])

#merged_dim=(latent_dim + latent_dim,)
#merged_inputs = Input(shape=merged_dim, name='encoder_input')
encoder = Dense(10,activation='relu')(merged_layer)
z_mean = Dense(10, name='z_mean')(encoder)
z_log_var = Dense(latent_dim, name='z_log_var')(encoder)
z = Lambda(sampling, name='z')([z_mean, z_log_var])

decoder_h1 = Dense(intermediate_dim, activation='relu')
decoder_h2 = Dense(original_dim, activation='sigmoid',name='decoder_h2')
                       
z_decoded = decoder_h1(z)
x_decoded = decoder_h2(z_decoded)

#%% instantiate VAE model

vae = Model(inputs=[dna_inputs,cna_inputs], outputs=[x_decoded_dna,x_decoded_cna,x_decoded])
plot_model(vae,to_file=os.path.join(NET_DIR,'vae_bn_network.png'), 
           show_shapes=True)

#%% compile VAE model

vae_loss=vae_objective(z_mean, z_log_var)
vae.compile(optimizer="adam", loss=vae_loss)

#%%
from keras.callbacks import History
hist_cb = History()
vae.fit(x_train, x_train, shuffle=True, epochs=epochs, batch_size=batch_size,
        callbacks=[hist_cb], validation_data=(x_dev, x_dev))
vae.save_weights(weights_file)