# -*- coding: utf-8 -*-
"""
Created on Wed Jun 13 20:31:16 2018

@author: Ifrah
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from keras.layers import Lambda, Input, Dense, BatchNormalization
from keras.models import Model
from keras.utils import plot_model



from params import input_shape, intermediate_dim, latent_dim, original_dim
from params import y_dev, epochs
from vae_functions import sampling, vae_objective

import os


#%%

PATH = os.getcwd()
NET_DIR = PATH + "\\vae\\cna\\toy"
LOG_DIR = NET_DIR + '\\tflogs'

#%%

metadata = os.path.join(LOG_DIR, 'metadata.tsv')
#add as many labels to the embedding as you would like.
#this is a label used to identify key points about the latent space
#that is being used to describe the patient
with open(metadata, 'w') as metadata_file:
    metadata_file.write('severity\tage\n')
    for label in y_dev:
        metadata_file.write('%d\t%d\n' % (label, label))

weights_file = os.path.join(LOG_DIR, 
               "_vae_bn_%d_latent_%d_epochs.hdf5") % (latent_dim,epochs)
#%%
# VAE model = encoder + decoder

#%% build encoder 
inputs = Input(shape=input_shape, name='encoder_input')

#BatchNormalization
#GaussianNoise

encoder_h = Dense(intermediate_dim, activation='relu')(inputs)
#this has no activation. it just takes h, does nothing to it
#and the output space has a dimensionality of latent_dim

#feed same inputs to these nodes. these will later be used to calculate 
#the z
z_mean = Dense(latent_dim, name='z_mean')(encoder_h)
z_log_var = Dense(latent_dim, name='z_log_var')(encoder_h)

# use reparameterization trick to push the sampling out as input
# note that "output_shape" isn't necessary with the TensorFlow backend
z = Lambda(sampling, name='z')([z_mean, z_log_var])

#%% build decoder 

decoder_h1 = Dense(intermediate_dim, activation='relu')
decoder_h2 = Dense(original_dim, activation='sigmoid',name='decoder_h2')

z_N=BatchNormalization()(z)
z_decoded = decoder_h1(z_N)
z_decoded_N=BatchNormalization()(z_decoded)
x_decoded = decoder_h2(z_decoded_N)

#%% instantiate VAE model

vae = Model(inputs=inputs, outputs=x_decoded, name='vae_mlp')
plot_model(vae,to_file=os.path.join(NET_DIR,'vae_bn_network.png'), 
           show_shapes=True)

#%% compile VAE model

vae_loss=vae_objective(z_mean, z_log_var)
vae.compile(optimizer="adam", loss=vae_loss)









