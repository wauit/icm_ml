


encoded_layer_model_lambda = Model(inputs=vae.input, outputs=vae.get_layer(index=-3).output)

encoded_output_lambda = encoded_layer_model_lambda.predict(x_train)

mu=np.mean(encoded_output_lambda,axis=0)
sigma=np.std(encoded_output_lambda,axis=0)

def KL(mu, sigma):
    kl=-0.5*( 1 + np.log(sigma) - np.square(mu) - sigma )
    return np.log(kl)

KL(mu,sigma)