# -*- coding: utf-8 -*-
"""
Created on Mon Jun 18 18:29:06 2018

@author: Ifrah
"""
import random
import numpy as np
np.random.seed(42)


#%%
class dnaEXPR:
    "This class creates and modifies the CNA dataset"
    def __init__(self):
        self.geneNum = 10
        self.data = np.array([]).reshape(0,self.geneNum)
        self.label = np.array([]).reshape(0,1)
        self.severity = np.array([]).reshape(0,1)
        self.exprlist = list(range(20,90,10))
        
    def __bioMarkers(self,os):
        if(os>=0 and os<20):
            FAB = random.sample(range(75,85), 3)
            DIE = random.sample(range(15,25), 3)
            CGHJ = random.sample(range(45,55), 4)
            severity = 0
            
        elif(os>=20 and os<40):
            FAB = random.sample(range(65,75), 3)
            DIE = random.sample(range(25,35), 3)
            CGHJ = random.sample(range(45,55), 4)
            severity = 1
            
        elif(os>=40):
            FAB = random.sample(range(55,65), 3)
            DIE = random.sample(range(35,45), 3)
            CGHJ = random.sample(range(45,55), 4)
            severity = 2
            
        CNApattern = np.array([FAB[1],FAB[2],CGHJ[0],
                               DIE[0],DIE[2],FAB[0],
                               CGHJ[1],CGHJ[2],DIE[1],CGHJ[3]]).reshape(1,10)
    
        return(CNApattern, severity) 
            
    def addPatient(self, os):
        d,s = self.__bioMarkers(os)
        self.data = np.vstack((self.data, d))
        self.labels = np.vstack((self.label, os))
        self.severity = np.vstack((self.severity,s))
        
        
