# -*- coding: utf-8 -*-
"""
Created on Wed Jun 13 22:18:47 2018

@author: Ifrah
"""

from ipywidgets import FloatSlider, interact
from params import noise_std
from vae_bn import generator

import matplotlib.pyplot as plt
import matplotlib.cm as cm


import numpy as np
np.random.seed(42)
#%%


#we will sample points within given standard deviations
element1= FloatSlider(min=-15, max=15, step=3, value=0)
element2 = FloatSlider(min=-15, max=15, step=3, value=0)

@interact(element1=element1, element2=element2)
def do_thumb(element1, element2):
    z_sample = np.array([[element1, element2]]) * noise_std
    x_decoded = generator.predict(z_sample)
    pattern = x_decoded[0].reshape(1,10)
    
    colormap = cm.ScalarMappable(cmap="jet")
    img = colormap.to_rgba(pattern)
    img = np.repeat(img, 5, axis=1)
    
    plt.figure(figsize=(11.5, 11.5))
    ax = plt.subplot(111)
    
    ax.imshow(img)
    plt.axis("off")