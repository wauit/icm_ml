# -*- coding: utf-8 -*-
"""
Created on Fri Jul 27 11:13:31 2018

@author: Ifrah
"""

import sys

from SALib.analyze import dgsm
from SALib.sample import finite_diff
from SALib.test_functions import Ishigami
from SALib.util import read_param_file

from vae_transcoder import *


#%%
def SA(model,
       data,
       perplexity = 30,
       batch_size=batch_size,
       model_name="vae_CNA"):
    """Plots labels and MNIST digits as function of 2-dim latent vector
    # Arguments:
        models (tuple): encoder and decoder models
        data (tuple): test data and label
        batch_size (int): prediction batch size
        model_name (string): which model is using this function
    """
    encoder = model
    x_dev = data
    
    z = encoder.predict(x_dev,verbose=1,batch_size=batch_size)
    
    
    
#    fig = plt.figure(figsize=(12, 10)) 
#    ax = fig.add_subplot(111, projection='3d')
#    m = cm.ScalarMappable(cmap=cm.jet)
#    m.set_array(y_dev_cna)
#    ax.scatter(X[:,0],X[:,1], c=y_dev_cna.ravel(),cmap=cm.jet)
#    plt.colorbar(m)
#    plt.savefig(os.path.join(NET_DIR,'trans-tSNE3d'))
    
#    plt.show() 
    
    return(z)
#%%
sensitivty   
problem = {
  'num_vars': 5,
  'names': ['x1', 'x2', 'x3', 'x4', 'x5'],
  'bounds': [[0,1]]*5
}

z=SA(model=merged_encoder, data=dev_list)
problem=z

# Generate samples
param_values = finite_diff.sample(problem, 1000, delta=0.001)
Y = generator.evaluate(param_values)
Si = dgsm.analyze(problem, param_values, Y, conf_level=0.95, print_to_console=False)

sys.path.append('../..')
problem = read_param_file('../../SALib/test_functions/params/Ishigami.txt')


# Run the "model" -- this will happen offline for external models


# Perform the sensitivity analysis using the model output
# Specify which column of the output file to analyze (zero-indexed)
# Returns a dictionary with keys 'vi', 'vi_std', 'dgsm', and 'dgsm_conf'
# e.g. Si['vi'] contains the sensitivity measure for each parameter, in
# the same order as the parameter file

# For comparison, Morris mu* < sqrt(v_i)
# and total order S_tot <= dgsm, following Sobol and Kucherenko (2009)