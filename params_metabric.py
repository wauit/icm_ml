# -*- coding: utf-8 -*-
"""
Created on Fri Jul 27 17:20:51 2018

@author: Ifrah
"""

import os
import pandas as pd
import numpy as np
from sklearn.preprocessing import MinMaxScaler 
from sklearn import preprocessing

PATH = os.getcwd()
NET_DIR = PATH + "\\vae\\transcoder\\metabric"
LOG_DIR = NET_DIR + '\\tflogs'

data_dir = PATH + "\\data\\"

from bric_tcga import bricCNA, bricDNA, bricCNA_label, bricDNA_label
from bric_tcga import tcgaCNA, tcgaDNA, tcgaCNA_label, tcgaDNA_label
from bric_tcga import y_dev_dna, y_train_dna, y_dev_cna, y_train_cna

#from bric_tcga_subset import bricCNA, bricDNA, bricCNA_label, bricDNA_label
#from bric_tcga_subset import tcgaCNA, tcgaDNA, tcgaCNA_label, tcgaDNA_label
#from bric_tcga_subset import y_dev_dna, y_train_dna, y_dev_cna, y_train_cna
#%%
#tcgaCNA.loc[1:tcgaCNA.shape[1]].loc[tcgaCNA.columns.values[1:tcgaCNA.shape[1]].isin(bricCNA.columns.values[1:bricCNA.shape[1]])]

bricDNA = bricDNA.loc[:,bricDNA.columns[bricDNA.columns.isin(bricCNA.columns)]]
bricCNA = bricCNA.loc[:,bricCNA.columns[bricCNA.columns.isin(bricDNA.columns)]]

tcgaDNA = tcgaDNA.loc[:,tcgaDNA.columns[tcgaDNA.columns.isin(tcgaCNA.columns)]]
tcgaCNA = tcgaCNA.loc[:,tcgaCNA.columns[tcgaCNA.columns.isin(tcgaDNA.columns)]]

#bricCNA.columns.values[1:bricCNA.shape[1]]
#tcgaCNA.columns.values[1:tcgaCNA.shape[1]]

#%%
x_train_cna = bricCNA[0:1880]
x_train_cna = x_train_cna.drop(columns=['patients'])
x_dev_cna = tcgaCNA[0:260]
x_dev_cna = x_dev_cna.drop(columns=['patients'])

x_train_dna = bricDNA[0:1880]
x_train_dna = x_train_dna.drop(columns=['patients'])
x_dev_dna = tcgaDNA[0:260]
x_dev_dna = x_dev_dna.drop(columns=['patients'])


#%% TRAINING DATASET -
x_train_cna = preprocessing.scale(x_train_cna.T).T
x_train_dna = preprocessing.scale(x_train_dna.T).T

train_scaler = preprocessing.MinMaxScaler()
x_train_cna = train_scaler.fit_transform(x_train_cna.T).T
x_train_dna = train_scaler.fit_transform(x_train_dna.T).T
#%% DEV DATASET - DNA
x_dev_cna = preprocessing.scale(x_dev_cna.values.T).T
x_dev_dna = preprocessing.scale(x_dev_dna.values.T).T

dev_scaler = preprocessing.MinMaxScaler()
x_dev_cna = dev_scaler.fit_transform(x_dev_cna.T).T
x_dev_dna = dev_scaler.fit_transform(x_dev_dna.T).T
#%%
subset=500
train = 20650
dev=14686

#%%
x_train_cna = x_train_cna[0:1880,0:subset]
x_dev_cna = x_dev_cna[0:260,0:subset]
x_train_dna = x_train_dna[0:1880,0:subset]
x_dev_dna = x_dev_dna[0:260,0:subset]
#%%

#x_train_cna = bricCNA.values[0:1880,1:subset]
##x_train_cna = x_train_cna.drop(columns=['patients'])
#x_dev_cna = tcgaCNA.values[0:260,1:subset]
##x_dev_cna = x_dev_cna.drop(columns=['patients'])
#
#x_train_dna = bricDNA.values[0:1880,1:subset]
##x_train_dna = x_train_dna.drop(columns=['patients'])
#x_dev_dna = tcgaDNA.values[0:260,1:subset]
##x_dev_dna = x_dev_dna.drop(columns=['patients'])
#%% TRAINING DATASET - CNA
#train_scaler = preprocessing.MinMaxScaler()
#x_train_cna = train_scaler.fit_transform(x_train_cna.T).T
#
#dev_scaler = preprocessing.MinMaxScaler()
#x_dev_cna = dev_scaler.fit_transform(x_dev_cna.T).T
#
#
##%% TRAINING DATASET - DNA
#train_scaler = preprocessing.MinMaxScaler()
#x_train_dna = train_scaler.fit_transform(x_train_dna.T).T
#
#dev_scaler = preprocessing.MinMaxScaler()
#x_dev_dna = dev_scaler.fit_transform(x_dev_dna.T).T

#%%
image_size = x_train_cna.shape[1]
original_dim=x_train_cna.shape[1]
input_shape=(original_dim,)



#%% CONCAT DATASET
x_train_st=np.concatenate((x_train_cna,x_train_dna), axis=1)
x_dev_st=np.concatenate((x_dev_cna,x_dev_dna), axis=1)

#%% PARAMETERS

cna_size = x_train_cna.shape[1]
cna_original_dim = x_train_cna.shape[1]

# network parameters
cna_input_shape = (cna_original_dim,) #(original_dim, )
cna_intermediate_dim = 500

dna_size = x_train_dna.shape[1]
dna_original_dim = x_train_dna.shape[1]

# network parameters
dna_input_shape = (dna_original_dim,) #(original_dim, )
dna_intermediate_dim = 500

image_size = x_train_cna.shape[1]
original_dim=x_train_cna.shape[1]
input_shape=(original_dim,)

batch_size = 10
latent_dim = 5
intermediate_dim = 500
epochs = 50
noise_std = 0  
