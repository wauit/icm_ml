# -*- coding: utf-8 -*-
"""
Created on Wed Jun 13 19:54:39 2018

@author: Ifrah
"""

from train_dev import cna_train, cna_dev, dna_train, dna_dev
import numpy as np
from sklearn.preprocessing import MinMaxScaler 
#%% TRAINING DATASET - CNA
train_scaler = MinMaxScaler()
x_train_cna = train_scaler.fit_transform(cna_train.data)
##undo transformation
#x_train = train_scaler.inverse_transform(x_train)
y_train_cna = cna_train.severity

dev_scaler = MinMaxScaler()
x_dev_cna = dev_scaler.fit_transform(cna_dev.data)
##undo transformation
#x_dev = dev_scaler.inverse_transform(x_dev)
y_dev_cna = cna_dev.severity

#%% TRAINING DATASET - dnaEXPR
train_scaler = MinMaxScaler()
x_train_dna = train_scaler.fit_transform(dna_train.data)
##undo transformation
#x_train = train_scaler.inverse_transform(x_train)
y_train_dna = dna_train.severity

dev_scaler = MinMaxScaler()
x_dev_dna = dev_scaler.fit_transform(dna_dev.data)
##undo transformation
#x_dev = dev_scaler.inverse_transform(x_dev)
y_dev_dna = dna_dev.severity
#%% CONCAT DATASET
x_train_st=np.concatenate((x_train_cna,x_train_dna), axis=1)
x_dev_st=np.concatenate((x_dev_cna,x_dev_dna), axis=1)

#%% PARAMETERS

cna_size = x_train_cna.shape[1]
cna_original_dim = cna_train.geneNum

# network parameters
cna_input_shape = (cna_original_dim,) #(original_dim, )
cna_intermediate_dim = 5

dna_size = x_train_dna.shape[1]
dna_original_dim = dna_train.geneNum

# network parameters
dna_input_shape = (dna_original_dim,) #(original_dim, )
dna_intermediate_dim = 5

image_size = x_train_cna.shape[1]
original_dim=x_train_cna.shape[1]
input_shape=(original_dim,)
batch_size = 10
latent_dim = 5
intermediate_dim = 10
epochs = 50
noise_std = .01   


