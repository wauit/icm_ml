# -*- coding: utf-8 -*-
"""
Created on Wed Jun 13 20:11:38 2018

@author: Ifrah
"""
from params import batch_size

import matplotlib.pyplot as plt
import matplotlib.cm as cm
from mpl_toolkits.axes_grid1 import make_axes_locatable

from ggplot import *
from sklearn.manifold import TSNE

import numpy as np

import os
#%%
def showPatients(data_matrix):
    fig = plt.figure(figsize=(12, 10)) 
    ax = fig.add_subplot(111)
    
    colormap = cm.ScalarMappable(cmap="jet")
    img = colormap.to_rgba(data_matrix)
    img = np.repeat(img, 5, axis=1)
    ax.imshow(img,interpolation='nearest', vmin=0.5, vmax=0.99)
    
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    colormap.set_array(data_matrix)
    fig.colorbar(colormap, cax=cax)

    fig.show()

#%%
def plot_first2D(models,
                 data,
                 batch_size=10,
                 model_name="vae_CNA"):
    """Plots labels and MNIST digits as function of 2-dim latent vector
    # Arguments:
        models (tuple): encoder and decoder models
        data (tuple): test data and label
        batch_size (int): prediction batch size
        model_name (string): which model is using this function
    """

    encoder, decoder = models
    x_test, y_test = data
    os.makedirs(model_name, exist_ok=True)

    filename = os.path.join(model_name, "vae_mean.png")
    # display a 2D plot of the digit classes in the latent space
    z_mean= encoder.predict(x_test,batch_size=batch_size)
    fig = plt.figure(figsize=(12, 10)) 
    ax = fig.add_subplot(111)
    m = cm.ScalarMappable(cmap=cm.jet)
    m.set_array(y_test)
    #.ravel() fixed the dimension/shape issue
    ax.scatter(x=z_mean[:, 0],y= z_mean[:, 1], c=y_test.ravel(),cmap=cm.jet)
    plt.colorbar(m)
    plt.xlabel("z[0]")
    plt.ylabel("z[1]")
    plt.savefig(filename)
    plt.show()

#%%
def plot_tSNE(models,
                 data,
                 perplexity = 30,
                 batch_size=batch_size,
                 model_name="vae_CNA"):
    """Plots labels and MNIST digits as function of 2-dim latent vector
    # Arguments:
        models (tuple): encoder and decoder models
        data (tuple): test data and label
        batch_size (int): prediction batch size
        model_name (string): which model is using this function
    """
    encoder, generator = models
    x_test, y_test = data
    
    z = encoder.predict(x_test,verbose=1,
                        batch_size=batch_size)

    tsne = TSNE(n_components=5,perplexity = perplexity, method='exact')
    X_tsne = tsne.fit_transform(z)
    
    fig = plt.figure(figsize=(12, 10)) 
    ax = fig.add_subplot(111)
    
    m = cm.ScalarMappable(cmap=cm.jet)
    m.set_array(y_test)
    ax.scatter(X_tsne[:, 0], X_tsne[:, 1], c=y_test.ravel(),cmap=cm.jet)
    plt.colorbar(m)
    plt.show() 

#%%
    
