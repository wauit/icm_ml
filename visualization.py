# -*- coding: utf-8 -*-
"""
Created on Wed Jun 20 12:52:15 2018

@author: Ifrah
"""
from keras.models import Model, Input


#from vae_dnaEXPR_toy import *
from vae_cna_toy import *

import matplotlib.pyplot as plt

import pandas as pd
import numpy as np
import string
import os

from vae_functions import KL_elem
#%%
x_df = pd.DataFrame(x_train_cna,columns=list(string.ascii_uppercase[0:10]))
x_bp = x_df.plot.box(return_type='both', patch_artist=True)

x_df_low = x_df[y_train_cna==0]
x_bp_low = x_df_low.plot.box(return_type='both', patch_artist=True, color="red")
x_df_low


x_df_med = x_df[y_train_cna==1]
x_bp_med = x_df_med.plot.box(return_type='both', patch_artist=True, color="gold")
x_df_med

x_df_high = x_df[y_train_cna==2]
x_bp_high = x_df_high.plot.box(return_type='both', patch_artist=True, color="green")
x_df_high

#%%
x_df = pd.DataFrame(x_train_dna,columns=list(string.ascii_uppercase[0:10]))
x_bp = x_df.plot.box(return_type='both', patch_artist=True)

x_df_low = x_df[y_train_dna==0]
x_bp_low = x_df_low.plot.box(return_type='both', patch_artist=True, color="red")
x_df_low


x_df_med = x_df[y_train_dna==1]
x_bp_med = x_df_med.plot.box(return_type='both', patch_artist=True, color="gold")
x_df_med

x_df_high = x_df[y_train_dna==2]
x_bp_high = x_df_high.plot.box(return_type='both', patch_artist=True, color="green")
x_df_high

#%%

fig_loss=plt.figure()
ax2=fig_loss.add_subplot(111)
ax2.plot(hist_cb.history["loss"], label="training")
ax2.plot(hist_cb.history["val_loss"], label="validation")
plt.grid("on")
plt.xlabel("epoch")
plt.ylabel("loss")
plt.legend(loc="best")

#%%

z=encoder.predict(x_dev)

z_mu=np.mean(z, axis=0)
z_var=np.std(z, axis=0)

KL_elem(z_mu,z_var)

#%%