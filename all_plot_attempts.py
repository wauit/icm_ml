# -*- coding: utf-8 -*-
"""
Created on Sun Jul 22 20:57:54 2018

@author: Ifrah
"""


from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os

from params import batch_size
from ggplot import *
from sklearn.manifold import TSNE

from vae_mergedAE import *
from keras.models import Model

import matplotlib.pyplot as plt
import matplotlib.cm as cm
from mpl_toolkits.axes_grid1 import make_axes_locatable
from sklearn.preprocessing import StandardScaler

import numpy as np

#%%
def plot_tSNE(model,
                 data,
                 perplexity = 30,
                 batch_size=batch_size,
                 model_name="vae_CNA"):
    """Plots labels and MNIST digits as function of 2-dim latent vector
    # Arguments:
        models (tuple): encoder and decoder models
        data (tuple): test data and label
        batch_size (int): prediction batch size
        model_name (string): which model is using this function
    """
    encoder = model
    x_dev = data
    
    z = encoder.predict(x_dev,verbose=1,batch_size=batch_size)

    tsne = TSNE(n_components=2,perplexity = 15)
    X_tsne = tsne.fit_transform(z)
    #X = X_tsne
    X = StandardScaler().fit_transform(X_tsne) 
    fig = plt.figure(figsize=(12, 10)) 
    ax = fig.add_subplot(111)
    m = cm.ScalarMappable(cmap=cm.jet)
    m.set_array(y_dev_cna)
    ax.scatter(X[:,0],X[:,1], c=y_dev_cna.ravel(),cmap=cm.jet)
    plt.colorbar(m)
    plt.show() 
    
    return(z,X)
    
#%%
plt.title('Merged VAE')
plt.plot(hist_cb.history["loss"], label="training")
plt.plot(hist_cb.history["val_loss"], label="validation")
plt.grid("off")
plt.xlabel("epoch")
plt.ylabel("loss")
plt.legend(loc="best")
plt.text(0,.115, r'$loss=%s,\ val\ loss=%s$'%(round(hist_cb.history["loss"][-1],3),
                                              round(hist_cb.history["val_loss"][-1],3)))
#%%

#z=merged_encoder.predict(dev_list,verbose=1,batch_size=batch_size)

#%%
z,tsne_pts=plot_tSNE(model=merged_encoder, data=dev_list)
unique, counts = np.unique(y_dev_cna, return_counts=True)
print (np.asarray((unique, counts)).T)

from sklearn.cluster import DBSCAN
db = DBSCAN(eps=0.3, min_samples=20).fit(tsne_pts)
core_samples = db.core_sample_indices_
labels = db.labels_
n_clusters = len(set(labels)) - (1 if -1 in labels else 0)
print(n_clusters)

clusters = [tsne_pts[labels == i] for i in range(n_clusters)]
patient_list = np.array(list(range(1000)))
patients = [patient_list[labels == i] for i in range(n_clusters)]

cnaDBSCAN = [dev_list[0][patients[i]] for i in range(n_clusters)]
dnaDBSCAN = [dev_list[1][patients[i]] for i in range(n_clusters)]

zspace = [z[patients[i]] for i in range(n_clusters)]

for i in range(n_clusters):
    x_df = pd.DataFrame(zspace[i])
    x_bp = x_df.plot.box(return_type='both', patch_artist=True)

#plot distribution of zspace
#then find mean of original zspace
z_center=[np.mean(zspace[i], axis=0) for i in range(n_clusters)]


clusters[0].shape
clusters[1].shape
clusters[2].shape
outliers = tsne_pts[labels == -1]


#%%
#data = tsne_pts
#labels = ['point{0}'.format(i) for i in range(data.shape[0])]
#
#plt.subplots_adjust(bottom = 0.1)
#plt.scatter(data[:,0],data[:,1], c=y_dev_cna.ravel(),cmap=cm.jet)
#
#for label, x, y in zip(labels, data[:, 0], data[:, 1]):
#    plt.annotate(
#        label,
#        xy=(x, y), xytext=(-20, 20),
#        textcoords='offset points', ha='right', va='bottom',
#        bbox=dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.5),
#        arrowprops=dict(arrowstyle = '->', connectionstyle='arc3,rad=0'))
#
#plt.show()

#%% affinity propagation
print(__doc__)

from sklearn.cluster import AffinityPropagation
from sklearn import metrics
from sklearn.datasets.samples_generator import make_blobs

# #############################################################################
# Generate sample data
X=tsne_pts
labels_true = y_dev_cna.reshape(1000,)
# #############################################################################
# Compute Affinity Propagation
af = AffinityPropagation().fit(X)
cluster_centers_indices = af.cluster_centers_indices_
labels = af.labels_

n_clusters_ = len(cluster_centers_indices)

print('Estimated number of clusters: %d' % n_clusters_)
print("Homogeneity: %0.3f" % metrics.homogeneity_score(labels_true, labels))
print("Completeness: %0.3f" % metrics.completeness_score(labels_true, labels))
print("V-measure: %0.3f" % metrics.v_measure_score(labels_true, labels))
print("Adjusted Rand Index: %0.3f"
      % metrics.adjusted_rand_score(labels_true, labels))
print("Adjusted Mutual Information: %0.3f"
      % metrics.adjusted_mutual_info_score(labels_true, labels))
print("Silhouette Coefficient: %0.3f"
      % metrics.silhouette_score(X, labels, metric='sqeuclidean'))

# #############################################################################
# Plot result
import matplotlib.pyplot as plt
from itertools import cycle

plt.close('all')
plt.figure(1)
plt.clf()

colors = cycle('bgrcmykbgrcmykbgrcmykbgrcmyk')
ClustCenter = [X[cluster_centers_indices[k]] for k in range(n_clusters_)]

result = []
for i in range(n_clusters):
    temp=[a for a in patients[i] if a in cluster_centers_indices]
    result.append(temp)

cnaClust = [dev_list[0][result[i]] for i in range(n_clusters)]
dnaClust = [dev_list[1][result[i]] for i in range(n_clusters)]



for k, col in zip(range(n_clusters_), colors):
    class_members = labels == k
    cluster_center = X[cluster_centers_indices[k]]
    plt.plot(X[class_members, 0], X[class_members, 1], col + '.')
    plt.plot(cluster_center[0], cluster_center[1], 'o', markerfacecolor=col,
             markeredgecolor='k', markersize=14)
    for x in X[class_members]:
        plt.plot([cluster_center[0], x[0]], [cluster_center[1], x[1]], col)

plt.title('Estimated number of clusters: %d' % n_clusters_)
plt.show()

#%%
import string
for i in range(3):
    x_df = pd.DataFrame(cnaClust[i],columns=list(string.ascii_uppercase[0:10]))
    x_bp = x_df.plot.box(return_type='both', patch_artist=True)

for i in range(3):
    x_df = pd.DataFrame(dnaClust[i],columns=list(string.ascii_uppercase[0:10]))
    x_bp = x_df.plot.box(return_type='both', patch_artist=True)

#%%
from sklearn.preprocessing import MinMaxScaler 
z_center = np.asarray(z_center)
#z_scalar = MinMaxScaler()
#z_center_scaled = z_scalar.fit_transform(z_center)
rep_patient=generator.predict(z_center)
rep_patient=np.asarray(rep_patient)
#%% generated patient group mean - CNA
i=0
import numpy as np
import matplotlib.pyplot as plt
x=list(range(0,10))
fig = plt.figure()
ax1 = fig.add_subplot(111)
ax1.scatter(x,rep_patient[i][0], c="r")
ax1.scatter(x,rep_patient[i][1], c="k")
ax1.scatter(x,rep_patient[i][2], c="c")
letters = list(string.ascii_uppercase[0:10])
plt.xticks(x, letters)
plt.show()

#%% generated patient group mean - DNA
i=1
x=list(range(0,10))
fig = plt.figure()
ax1 = fig.add_subplot(111)
ax1.scatter(x,rep_patient[i][0], c="r")
ax1.scatter(x,rep_patient[i][1], c="k")
ax1.scatter(x,rep_patient[i][2], c="c")
plt.xticks(x, letters)
plt.show()

#%%
x_df = pd.DataFrame(rep_patient[0][2],columns=list(string.ascii_uppercase[0:10]))
x_bp = x_df.plot.box(return_type='both', patch_artist=True)
#%%
#def fit(self):
#    self.graph = tf.Graph()
#    with self.graph.as_default():
#        self.X = tf.placeholder(self.dtype, shape=self.data.shape)
#
#        # Perform SVD
#        singular_values, u, _ = tf.svd(self.X)
#
#        # Create sigma matrix
#        sigma = tf.diag(singular_values)
#
#    with tf.Session(graph=self.graph) as session:
#        self.u, self.singular_values, self.sigma = session.run([u, singular_values, sigma],
#                                                               feed_dict={self.X: self.data})
#        
##%%
#def reduce(self, n_dimensions=None, keep_info=None):
#    if keep_info:
#        # Normalize singular values
#        normalized_singular_values = self.singular_values / sum(self.singular_values)
#        # Create the aggregated ladder of kept information per dimension
#        ladder = np.cumsum(normalized_singular_values)
#        # Get the first index which is above the given information threshold
#        index = next(idx for idx, value in enumerate(ladder) if value >= keep_info) + 1
#        n_dimensions = index
#    with self.graph.as_default():
#        # Cut out the relevant part from sigma
#        sigma = tf.slice(self.sigma, [0, 0], [self.data.shape[1], n_dimensions])
#        # PCA
#        pca = tf.matmul(self.u, sigma)
#    with tf.Session(graph=self.graph) as session:
#        return session.run(pca, feed_dict={self.X: self.data})
#
##%%
#from sklearn import datasets
#import matplotlib.pyplot as plt
#import seaborn as sns
#tf_pca = TF_PCA(iris_dataset.data, iris_dataset.target)
#tf_pca.fit()
#pca = tf_pca.reduce(keep_info=0.9)  # Results in 2 dimensions
#color_mapping = {0: sns.xkcd_rgb['bright purple'], 1: sns.xkcd_rgb['lime'], 2: sns.xkcd_rgb['ochre']}
#colors = list(map(lambda x: color_mapping[x], tf_pca.target))
#plt.scatter(pca[:, 0], pca[:, 1], c=colors)

#%%
x_df = pd.DataFrame(rep_patient[0][2],columns=list(string.ascii_uppercase[0:10]))
x_bp = x_df.plot.box(return_type='both', patch_artist=True)
#%%
import pprint
spectralnn = cluster.SpectralClustering(n_clusters=2,
                                eigen_solver='arpack',
                                affinity='nearest_neighbors',
                                n_neighbors=10)
pp = pprint.PrettyPrinter(indent=4)
pp.pprint(spectralnn.get_params())
fit_and_plot(spectralnn,"Spectral clustering on two circles")

W = spectralnn.affinity_matrix_
G=nx.from_scipy_sparse_matrix(W)
nx.draw(G,X,node_color='g',node_size=150)
plt.axis('equal')
plt.show()


#%%
from sklearn.decomposition import PCA
X = z
scikit_pca = PCA(n_components=2)
X_spca = scikit_pca.fit_transform(X)

fig = plt.figure(figsize=(12, 10)) 
ax = fig.add_subplot(111)
#    m = cm.ScalarMappable(cmap=cm.jet)
#    m.set_array(y_dev_cna)
ax.scatter(X_spca[:,0],X_spca[:,1], c=y_dev_cna.ravel(),cmap=cm.jet)


fig = plt.figure(figsize=(5, 5)) 
ax = fig.add_subplot(111)
#    m = cm.ScalarMappable(cmap=cm.jet)
#    m.set_array(y_dev_cna)
ax.scatter(z[:,1],z[:,3])


fig = plt.figure(figsize=(5, 5)) 
ax = fig.add_subplot(111)
#    m = cm.ScalarMappable(cmap=cm.jet)
#    m.set_array(y_dev_cna)
#ax.scatter(x_dev_dna[1,:],x_dev_dna[3,:])
ax.scatter(x_dev_dna[1,:],x_dev_dna[3,:])

#%%get the clusters DBSCAN
clusters = [tsne_pts[labels == i] for i in range(n_clusters)]
patient_list = np.array(list(range(1000)))
patients = [patient_list[labels == i] for i in range(n_clusters)]

cnaDBSCAN = [x_dev_st[:,0:10][patients[i]] for i in range(n_clusters)]
dnaDBSCAN = [x_dev_st[:,10:20][patients[i]] for i in range(n_clusters)]

zspace = [z[patients[i]] for i in range(n_clusters)]

#histograms of the 
for i in range(n_clusters):
    
    x_df = pd.DataFrame(zspace[i])
    x_bp = x_df.plot.box(return_type='both', patch_artist=True)

#plot distribution of zspace
#then find mean of original zspace
z_center=[np.mean(zspace[i], axis=0) for i in range(n_clusters)]


clusters[0].shape
clusters[1].shape
clusters[2].shape
outliers = tsne_pts[labels == -1]

#%% affinity propagation
print(__doc__)

from sklearn.cluster import AffinityPropagation
from sklearn import metrics
from sklearn.datasets.samples_generator import make_blobs

# #############################################################################
# Generate sample data
X=tsne_pts
labels_true = y_dev_cna.reshape(1000,)
# #############################################################################
# Compute Affinity Propagation
af = AffinityPropagation().fit(X)
cluster_centers_indices = af.cluster_centers_indices_
labels = af.labels_

n_clusters_ = len(cluster_centers_indices)

print('Estimated number of clusters: %d' % n_clusters_)
print("Homogeneity: %0.3f" % metrics.homogeneity_score(labels_true, labels))
print("Completeness: %0.3f" % metrics.completeness_score(labels_true, labels))
print("V-measure: %0.3f" % metrics.v_measure_score(labels_true, labels))
print("Adjusted Rand Index: %0.3f"
      % metrics.adjusted_rand_score(labels_true, labels))
print("Adjusted Mutual Information: %0.3f"
      % metrics.adjusted_mutual_info_score(labels_true, labels))
print("Silhouette Coefficient: %0.3f"
      % metrics.silhouette_score(X, labels, metric='sqeuclidean'))

# #############################################################################
# Plot result
import matplotlib.pyplot as plt
from itertools import cycle

plt.close('all')
plt.figure(1)
plt.clf()

colors = cycle('bgrcmykbgrcmykbgrcmykbgrcmyk')
ClustCenter = [X[cluster_centers_indices[k]] for k in range(n_clusters_)]

result = []
for i in range(n_clusters):
    temp=[a for a in patients[i] if a in cluster_centers_indices]
    result.append(temp)

cnaClust = [x_dev_st[:,0:10][result[i]] for i in range(n_clusters)]
dnaClust = [x_dev_st[:,10:20][result[i]] for i in range(n_clusters)]

for k, col in zip(range(n_clusters_), colors):
    class_members = labels == k
    cluster_center = X[cluster_centers_indices[k]]
    plt.plot(X[class_members, 0], X[class_members, 1], col + '.')
    plt.plot(cluster_center[0], cluster_center[1], 'o', markerfacecolor=col,
             markeredgecolor='k', markersize=14)
    for x in X[class_members]:
        plt.plot([cluster_center[0], x[0]], [cluster_center[1], x[1]], col)

plt.title('Estimated number of clusters: %d' % n_clusters_)
#plt.show()
plt.savefig(os.path.join(NET_DIR,'trans-AP'))