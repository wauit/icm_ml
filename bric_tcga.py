# -*- coding: utf-8 -*-
"""
Created on Sat Jul 28 17:20:36 2018

@author: Ifrah
"""
#%%
import numpy as np
from sklearn.preprocessing import MinMaxScaler 

import os
import pandas as pd

#%%
PATH = os.getcwd()
NET_DIR = PATH + "\\vae\\transcoder\\metabric"
LOG_DIR = NET_DIR + '\\tflogs'

data_dir = PATH + "\\data\\"
#%%
bricCNA = pd.read_csv(os.path.join(data_dir,'bric_CNA.txt'), delimiter=',')
bricCNA_label = pd.read_csv(os.path.join(data_dir,'bric_CNA-label.txt'), delimiter=',', index_col=0)

bricCNA.rename( columns={'Unnamed: 0':'patients'}, inplace=True )
bricCNA_label.rename( columns={'PATIENT_ID':'patients'}, inplace=True )

bricCNA = bricCNA.dropna()
bricCNA_label=bricCNA_label.loc[bricCNA_label.patients.isin(bricCNA.patients)]

#%%
bricDNA = pd.read_csv(os.path.join(data_dir,'bric_DNA.txt'), delimiter=',')
bricDNA_label = pd.read_csv(os.path.join(data_dir,'bric_DNA-label.txt'), delimiter=',', index_col=0)

bricDNA.rename( columns={'Unnamed: 0':'patients'}, inplace=True )
bricDNA_label.rename( columns={'PATIENT_ID':'patients'}, inplace=True )

bricDNA = bricDNA.dropna()
bricCNA = bricCNA.loc[bricCNA.patients.isin(bricDNA.patients)]
bricDNA = bricDNA.loc[bricDNA.patients.isin(bricCNA.patients)]


bricDNA_label=bricDNA_label.loc[bricDNA_label.patients.isin(bricDNA.patients)]
bricCNA_label=bricCNA_label.loc[bricCNA_label.patients.isin(bricCNA.patients)]
#%%
tcgaCNA = pd.read_csv(os.path.join(data_dir,'tcga_CNA.txt'), delimiter=',')
tcgaCNA_label = pd.read_csv(os.path.join(data_dir,'tcga_CNA-label.txt'), delimiter=',',index_col=0)

tcgaCNA.rename( columns={'Unnamed: 0':'patients'}, inplace=True)
tcgaCNA_label.rename( columns={'PATIENT_ID':'patients'}, inplace=True)

tcgaCNA = tcgaCNA.dropna()

tcgaCNA_label=tcgaCNA_label.loc[tcgaCNA_label.patients.isin(tcgaCNA.patients)]
tcgaCNA=tcgaCNA.loc[tcgaCNA.patients.isin(tcgaCNA_label.patients)]
#%%
tcgaDNA = pd.read_csv(os.path.join(data_dir,'tcga_DNA.txt'), delimiter=',')
tcgaDNA_label = pd.read_csv(os.path.join(data_dir,'tcga_DNA-label.txt'), delimiter=',',index_col=0)

tcgaDNA.rename( columns={'Unnamed: 0':'patients'}, inplace=True )
tcgaDNA_label.rename( columns={'PATIENT_ID':'patients'}, inplace=True )

tcgaDNA = tcgaDNA.dropna()

tcgaDNA_label=tcgaDNA_label.loc[tcgaDNA_label.patients.isin(tcgaDNA.patients)]
tcgaDNA=tcgaDNA.loc[tcgaDNA.patients.isin(tcgaDNA_label.patients)]
#%%
tcgaCNA = tcgaCNA.loc[tcgaCNA.patients.isin(tcgaDNA.patients)]
tcgaCNA_label=tcgaCNA_label.loc[tcgaCNA_label.patients.isin(tcgaCNA.patients)]

tcgaDNA = tcgaDNA.loc[tcgaDNA.patients.isin(tcgaCNA.patients)]
tcgaDNA_label=tcgaDNA_label.loc[tcgaDNA_label.patients.isin(tcgaDNA.patients)]
#%%
bricCNA_label = bricCNA_label[0:1880]

ER = {'neg': 0,'pos': 1}
y_train_cna = bricCNA_label['ER_IHC'].map(ER, na_action='ignore')
y_train_cna = y_train_cna.fillna(2)


#y_train_cna = pd.cut(bricCNA_label['OS_MONTHS'], [0,20,40,60,80,100,120,140,160,180,200], labels=['0', '1', '2','3','4','5','6','7','8','9']).values.add_categories('10')
#y_train_cna=y_train_cna.fillna('10') 
#%%
tcgaCNA_label = tcgaCNA_label[0:260]

ER = {'Negative': 0,'Positive': 1}
y_dev_cna = tcgaCNA_label['ER_STATUS_BY_IHC'].map(ER, na_action='ignore')
y_dev_cna = y_dev_cna.fillna(2)

#y_dev_cna = pd.cut(pd.to_numeric(tcgaCNA_label['OS_MONTHS'], errors='coerce'), [0,20,40,60,80,100,120,140,160,180,200], labels=['0', '1', '2','3','4','5','6','7','8','9']).values.add_categories('10')
#y_dev_cna=y_dev_cna.fillna('10') 
#%%
bricDNA_label = bricDNA_label[0:1880]

ER = {'neg': 0,'pos': 1}
y_train_dna = bricDNA_label['ER_IHC'].map(ER, na_action='ignore')
y_train_dna = y_train_cna.fillna(2)

#y_train_dna = pd.cut(bricDNA_label['OS_MONTHS'], [0,20,40,60,80,100,120,140,160,180,200], labels=['0', '1', '2','3','4','5','6','7','8','9']).values.add_categories('10')
#y_train_dna=y_train_dna.fillna('10') 
#%%
tcgaDNA_label = tcgaDNA_label[0:260]

ER = {'Negative': 0,'Positive': 1}
y_dev_dna = tcgaCNA_label['ER_STATUS_BY_IHC'].map(ER, na_action='ignore')
y_dev_dna = y_dev_dna.fillna(2)

#y_dev_dna = pd.cut(pd.to_numeric(tcgaDNA_label['OS_MONTHS'], errors='coerce'), [0,20,40,60,80,100,120,140,160,180,200], labels=['0', '1', '2','3','4','5','6','7','8','9']).values.add_categories('10')
#y_dev_dna=y_dev_dna.fillna('10')

#%%
tcgaIntclust = pd.read_csv(os.path.join(data_dir,'TCGAIntClust1100.txt'), delimiter='\t')
tcgaIntclust.rename( columns={'ID':'patients'}, inplace=True)
tcgaIntclust["patients"]=tcgaIntclust["patients"].str.replace("-01A","")
y_dev=tcgaIntclust.loc[tcgaIntclust.patients.isin(tcgaDNA.patients)]
