# -*- coding: utf-8 -*-
"""
Created on Mon Jul 30 16:58:35 2018

@author: Ifrah
"""

# load libraries and set plot parameters
import numpy as np
import prettytable as pt
import pprint
import networkx as nx

import matplotlib.pyplot as plt

from sklearn import cluster, datasets
np.random.seed(0)
n_samples = 400
X, y = datasets.make_circles(n_samples=n_samples, factor=.4, noise=.1)

plt.title("two circles data")
plt.plot(X[:,0],X[:,1],'go')
plt.xlabel("$x_1$")
plt.ylabel("$x_2$")
plt.axes().set_aspect('equal')

def fit_and_plot(algorithm,title):
    col = ['bo','ro','co', 'mo','ko']
    algorithm.fit(X)
    n_clusters = algorithm.n_clusters
    lab = algorithm.labels_
    reds = lab == 0
    blues = lab == 1
    for jj in range(n_clusters):
        plt.plot(X[lab == jj, 0], X[lab == jj, 1], col[jj])
    plt.xlabel("$x_1$")
    plt.ylabel("$x_2$")
    plt.title(title)
    plt.axes().set_aspect('equal')    
    
#run k-means
kmeans = cluster.KMeans(n_clusters=2)
fit_and_plot(kmeans, "K-means on two circles")



spectralnn = cluster.SpectralClustering(n_clusters=2,
                                eigen_solver='arpack',
                                affinity='nearest_neighbors',
                                n_neighbors=10)
pp = pprint.PrettyPrinter(indent=4)
pp.pprint(spectralnn.get_params())
fit_and_plot(spectralnn,"Spectral clustering on two circles")


W = spectralnn.affinity_matrix_
G=nx.from_scipy_sparse_matrix(W)
nx.draw(G,X,node_color='g',node_size=150)
plt.axis('equal')
plt.show()


