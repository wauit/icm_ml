# -*- coding: utf-8 -*-
"""
Created on Fri Jun 29 13:53:55 2018

@author: Ifrah
"""

#%%
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from keras.layers import Lambda, Input, Dense, BatchNormalization, concatenate
from keras.callbacks import TensorBoard
from keras.models import Model
from keras.utils import plot_model

from params import x_dev_cna, x_dev_dna, x_train_cna, x_train_dna
from params import batch_size, dna_input_shape, cna_input_shape
from params import y_dev_cna, y_dev_dna, dna_intermediate_dim, cna_intermediate_dim
from params import intermediate_dim, latent_dim, epochs, cna_original_dim, dna_original_dim
from params import y_train_cna, y_train_dna
from vae_functions import sampling, vae_objective

from plotting import plot_tSNE

import tensorflow as tf
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os

#%%
PATH = os.getcwd()
NET_DIR = PATH + "\\vae\\merged\\toy"
LOG_DIR = NET_DIR + '\\tflogs'

#%%
patientkey=pd.Series(list(range(0, 1000)))
severity = pd.Series([int(i) for i in y_dev_cna.ravel()])
df_ = pd.DataFrame({"patient": patientkey,"severity":severity})
metadata = os.path.join(LOG_DIR, 'metadata.tsv')
#add as many labels to the embedding as you would like.
#this is a label used to identify key points about the latent space
#that is being used to describe the patient
df_.to_csv(metadata, sep='\t',encoding='utf-8', index = False)

weights_file = os.path.join(LOG_DIR, 
               "_vae_dna_%d_latent_%d_epochs.hdf5") % (latent_dim,epochs)

#%% build encoder 
dna_inputs = Input(shape=dna_input_shape, name='dna_input')
cna_inputs = Input(shape=cna_input_shape, name='cna_input')

encoder_dna = Dense(dna_intermediate_dim, activation='relu')(dna_inputs)
encoder_cna = Dense(cna_intermediate_dim, activation='relu')(cna_inputs)

merged_layer=concatenate([encoder_cna, encoder_dna])
encoder_mixed = Dense(intermediate_dim, activation='relu')(merged_layer)

z_mean_mixed = Dense(latent_dim, name='z_mean_dna')(encoder_mixed)
z_log_var_mixed = Dense(latent_dim, name='z_log_var_dna')(encoder_mixed)


# use reparameterization trick to push the sampling out as input
# note that "output_shape" isn't necessary with the TensorFlow backend
z_mixed = Lambda(sampling, name='z_dna')([z_mean_mixed, z_log_var_mixed])


#%% build decoder
#decoder_mixed_h1 = Dense(intermediate_dim, activation='relu')
#decoder_mixed_h2 = Dense(original_dim, activation='sigmoid',name='decoder_mixed_h2')
#
#
#z_decoded_mixed = decoder_mixed_h1(z_mixed)
#x_decoded_mixed = decoder_mixed_h2(z_decoded_mixed)

decoder_cna_h1 = Dense(cna_intermediate_dim, activation='relu')
decoder_cna_h2 = Dense(cna_original_dim, activation='sigmoid',name='decoder_cna_h2')

z_decoded_cna = decoder_cna_h1(z_mixed)
x_decoded_cna = decoder_cna_h2(z_decoded_cna)

decoder_dna_h1 = Dense(dna_intermediate_dim, activation='relu')
decoder_dna_h2 = Dense(dna_original_dim, activation='sigmoid',name='decoder_dna_h2')

z_decoded_dna = decoder_dna_h1(z_mixed)
x_decoded_dna = decoder_dna_h2(z_decoded_dna)

#%% instantiate VAE model

vae = Model(inputs=[cna_inputs, dna_inputs], outputs=[x_decoded_cna, x_decoded_dna])

plot_model(vae,to_file=os.path.join(NET_DIR,'vae_merge.png'), 
           show_shapes=True)

#%%encoder
merged_encoder = Model(inputs=[cna_inputs, dna_inputs], outputs=z_mixed)

#%%generator
z_input = Input(shape=(latent_dim,))

decoder_cna_h1 = Dense(cna_intermediate_dim, activation='relu')
decoder_cna_h2 = Dense(cna_original_dim, activation='sigmoid',name='decoder_cna_h2')

z_decoded_cna = decoder_cna_h1(z_input)
x_decoded_cna = decoder_cna_h2(z_decoded_cna)

decoder_dna_h1 = Dense(dna_intermediate_dim, activation='relu')
decoder_dna_h2 = Dense(dna_original_dim, activation='sigmoid',name='decoder_dna_h2')

z_decoded_dna = decoder_dna_h1(z_input)
x_decoded_dna = decoder_dna_h2(z_decoded_dna)

generator = Model(inputs=z_input, outputs=[x_decoded_cna,x_decoded_dna])

#%% compile VAE model

vae_loss=vae_objective(z_mean_mixed, z_log_var_mixed)
vae.compile(optimizer="adam", loss=vae_loss)

#%%

train_list = [np.array(x_train_cna), np.array(x_train_dna)]
dev_list = [np.array(x_dev_cna), np.array(x_dev_dna)]
#%%
embed_freq=1
tb_callback=TensorBoard(log_dir=LOG_DIR, 
                        batch_size=batch_size,
                        #save layer output at x epoch 
                        embeddings_freq=embed_freq,
                        embeddings_metadata='metadata.tsv',
                        embeddings_data=dev_list,
                        embeddings_layer_names=['z_dna'],
                        histogram_freq=1,
                        write_grads=True)

#tensorboard --host localhost --logdir=E:\\Workspace\\MPhil_workspace\\ICM_ML\\vae\\merged\\toy\\tflogs

#%%

from keras.callbacks import History
hist_cb = History()

vae.fit(x=train_list,y=train_list, 
        shuffle=True, epochs=epochs, batch_size=batch_size,
        callbacks=[hist_cb], 
        validation_data=(dev_list, dev_list))
vae.save_weights(weights_file)



#%%

plt.title('Merged VAE')
plt.plot(hist_cb.history["loss"], label="training")
plt.plot(hist_cb.history["val_loss"], label="validation")
plt.grid("off")
plt.xlabel("epoch")
plt.ylabel("loss")
plt.legend(loc="best")
plt.text(0,.115, r'$loss=%s,\ val\ loss=%s$'%(round(hist_cb.history["loss"][-1],3),
                                              round(hist_cb.history["val_loss"][-1],3)))


