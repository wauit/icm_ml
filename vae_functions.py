# -*- coding: utf-8 -*-
"""
Created on Wed Jun 13 20:29:04 2018

@author: Ifrah
"""
from keras.losses import mean_squared_error
from keras import backend as K

#from params import batch_size, latent_dim, noise_std
from params_metabric import batch_size, latent_dim, noise_std

import numpy as np
#%%SAMPLING FROM LATENT SPACE
# reparameterization trick
# instead of sampling from Q(z|X), sample eps = N(0,I)
# z = z_mean + sqrt(var)*eps
def sampling(args):
    """Reparameterization trick by sampling from an isotropic unit Gaussian.
    # Arguments:
        args (tensor): mean and log of variance of Q(z|X)
    # Returns:
        z (tensor): sampled latent vector
    """

    z_mean, z_log_var= args
    z = K.random_normal(shape=(batch_size, latent_dim),
                                    mean=0., stddev=noise_std)
    z *= K.exp(.5 * z_log_var)
    z += z_mean
    # by default, random_normal has mean=0 and std=1.0
    return(z)
    
def kl_regu(z_mean,z_log_var):
    beta=0.5
    #regularizer. this is the KL of q(z|x) given that the 
    #distribution is N(0,1) (or any known distribution)
    kl_regu =   -beta * K.mean(1 + z_log_var - K.square(z_mean) - 
                K.exp(z_log_var), axis=-1)
    return(kl_regu)
    
def vae_objective(z_mean, z_log_var):
    def loss(x,x_decoded):
        loss = mean_squared_error(x, x_decoded)
        return(loss+kl_regu(z_mean,z_log_var))
    return(loss)

def KL_elem(mu,sigma):
    kl=-0.5*( 1 + np.log(sigma) - np.square(mu) - sigma )
    return(np.log(kl))
    
