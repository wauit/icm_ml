# -*- coding: utf-8 -*-
"""
Created on Wed Jun 13 19:37:14 2018

@author: Ifrah
"""

from keras.layers import Input
from keras.callbacks import TensorBoard
from keras.models import Model
from keras.utils import plot_model

from train_dev import train
from params import x_dev, y_dev, batch_size, x_train, epochs, input_shape
from params import latent_dim
from vae_functions import KL_elem
from plotting import plot_first2D, plot_tSNE, showPatients

from vae_dnaEXPR_toy import vae
from vae_dnaEXPR_toy import LOG_DIR, NET_DIR, weights_file
from vae_dnaEXPR_toy import *

import matplotlib.pyplot as plt

import pandas as pd
import numpy as np
import string
import os

np.random.seed(42)
#%%

x_df = pd.DataFrame(train.data,columns=list(string.ascii_uppercase[0:10]))
x_bp = x_df.plot.box(return_type='both', patch_artist=True)

x_df_low = x_df[train.severity==0]
x_bp_low = x_df_low.plot.box(return_type='both', patch_artist=True, color="red")
x_df_low


x_df_med = x_df[train.severity==1]
x_bp_med = x_df_med.plot.box(return_type='both', patch_artist=True, color="gold")
x_df_med

x_df_high = x_df[train.severity==2]
x_bp_high = x_df_high.plot.box(return_type='both', patch_artist=True, color="green")
x_df_high




#%%

tb_callback=TensorBoard(log_dir=LOG_DIR, 
                        batch_size=batch_size,
                        #save layer output at x epoch 
                        embeddings_freq=1,
                        embeddings_metadata='metadata.tsv',
                        embeddings_data=x_dev,
                        embeddings_layer_names=['z'],
                        histogram_freq=1,
                        write_grads=True)

#%%

#if os.path.isfile(weights_file):
#    vae.load_weights(weights_file)
#else:
from keras.callbacks import History
hist_cb = History()
vae.fit(x_train, x_train, shuffle=True, epochs=epochs, batch_size=batch_size,
        callbacks=[hist_cb, tb_callback], validation_data=(x_dev, x_dev))
vae.save_weights(weights_file)
    #hist_cb.history.keys()

#%%
# plot convergence curves to show off
fig_loss=plt.figure()
ax2=fig_loss.add_subplot(111)
ax2.plot(hist_cb.history["loss"], label="training")
ax2.plot(hist_cb.history["val_loss"], label="validation")
plt.grid("on")
plt.xlabel("epoch")
plt.ylabel("loss")
plt.legend(loc="best")

#%%
#encoder = Model(input=vae.input, output=vae.get_layer(index=-5).output)
#
#decoder_input = Input(shape=(latent_dim,))
#h_decoded = decoder_h1(decoder_input)
#x_decoded = decoder_h2(h_decoded)
#generator = Model(input=decoder_input, output=x_decoded)

#%%

#z=encoder.predict(x_dev)
#
#z_mu=np.mean(z, axis=0)
#z_var=np.std(z, axis=0)
#
#KL_elem(z_mu,z_var)

#%%
#models = (encoder, generator)
#data = (x_dev, y_dev)
#
#plot_first2D(models,
#             data,
#             batch_size=batch_size,
#             model_name="vae_mlp")
#    
#p1=plot_tSNE(models,
#             data,
#             batch_size=batch_size,
#             model_name="vae_mlp", perplexity=5)

#%%
showPatients(train.data[1:10])

#%%