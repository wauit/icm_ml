# -*- coding: utf-8 -*-
"""
Created on Wed Jun 13 20:29:04 2018

@author: Ifrah
"""

from keras import backend as K

from params import batch_size, latent_dim, noise_std
#%%SAMPLING FROM LATENT SPACE
# reparameterization trick
# instead of sampling from Q(z|X), sample eps = N(0,I)
# z = z_mean + sqrt(var)*eps
def sampling(args):
    """Reparameterization trick by sampling from an isotropic unit Gaussian.
    # Arguments:
        args (tensor): mean and log of variance of Q(z|X)
    # Returns:
        z (tensor): sampled latent vector
    """

    z_mean, z_log_var= args
    z = K.random_normal(shape=(batch_size, latent_dim),
                                    mean=0., stddev=noise_std)
    z *= K.exp(.5 * z_log_var)
    z += z_mean
    # by default, random_normal has mean=0 and std=1.0
    return(z)