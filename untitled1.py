# -*- coding: utf-8 -*-
"""
Created on Fri Jul 27 14:44:19 2018

@author: Ifrah
"""

import pandas as pd


PATH = os.getcwd()
NET_DIR = PATH + "\\vae\\transcoder\\metabric"
LOG_DIR = NET_DIR + '\\tflogs'

data_dir = PATH + "\\data\\"

df = pd.read_csv(os.path.join(NET_DIR,'bric_CNA.txt'), delimiter=',')