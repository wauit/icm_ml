# -*- coding: utf-8 -*-
"""
Created on Mon Jul 23 00:53:55 2018

@author: Ifrah
"""


from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os

from params import batch_size
from ggplot import *
from sklearn.manifold import TSNE

from vae_transcoder import *
from keras.models import Model

import matplotlib.pyplot as plt
import matplotlib.cm as cm
from mpl_toolkits.axes_grid1 import make_axes_locatable
from sklearn.preprocessing import StandardScaler
from mpl_toolkits.mplot3d import Axes3D

import numpy as np

#%%
def plot_tSNE(model,
                 data,
                 perplexity = 30,
                 batch_size=batch_size,
                 model_name="vae_CNA"):
    """Plots labels and MNIST digits as function of 2-dim latent vector
    # Arguments:
        models (tuple): encoder and decoder models
        data (tuple): test data and label
        batch_size (int): prediction batch size
        model_name (string): which model is using this function
    """
    encoder = model
    x_dev = data
    
    z = encoder.predict(x_dev,verbose=1,batch_size=batch_size)

    tsne = TSNE(n_components=2,perplexity = 50)
    X_tsne = tsne.fit_transform(z)
    #X = X_tsne
    X = StandardScaler().fit_transform(X_tsne) 
    fig = plt.figure(figsize=(12, 10)) 
    ax = fig.add_subplot(111, projection='3d')
    m = cm.ScalarMappable(cmap=cm.jet)
    m.set_array(y_dev_cna)
    ax.scatter(X[:,0],X[:,1], c=y_dev_cna.ravel(),cmap=cm.jet)
    plt.colorbar(m)
    plt.title("VAE - Cross Modal", fontsize=20)
    plt.savefig(os.path.join(NET_DIR,'trans-tSNE3d'))
    

    fig = plt.figure(figsize=(12, 10)) 
    ax = fig.add_subplot(111)
    m = cm.ScalarMappable(cmap=cm.jet)
    m.set_array(y_dev_cna)
    ax.scatter(X[:,0],X[:,1], c=y_dev_cna.ravel(),cmap=cm.jet)
    plt.colorbar(m)
    plt.title("VAE - Cross Modal", fontsize=20)
    plt.savefig(os.path.join(NET_DIR,'trans-tSNE2d'))
#    plt.show() 
    
    return(z,X)
    
#%%
plt.title('VAE - Cross Modal',fontsize=20)
plt.plot(hist_cb.history["decoder_cna_h2_loss"], label="CNA training")
plt.plot(hist_cb.history["decoder_dna_h2_loss"], label="GE training")
plt.plot(hist_cb.history["val_decoder_cna_h2_loss"], label="CNA validation")
plt.plot(hist_cb.history["val_decoder_dna_h2_loss"], label="GE validation")
plt.xlabel("epoch",fontsize=15)
plt.ylabel("loss",fontsize=15)
plt.legend(loc="best")
plt.text(3.5,0.065, r'$CNA \ loss=%s,\ CNA \ val\ loss=%s$'%(round(hist_cb.history["decoder_cna_h2_loss"][-1],3),
                                              round(hist_cb.history["val_decoder_cna_h2_loss"][-1],3)),fontsize=15)
plt.text(0,0.001, r'$GE \  loss=%s,\ GE \ val\ loss=%s$'%(round(hist_cb.history["decoder_dna_h2_loss"][-1],3),
                                              round(hist_cb.history["val_decoder_dna_h2_loss"][-1],3)),fontsize=15)
axes = plt.gca()
axes.set_ylim([0,.15])
plt.savefig(os.path.join(NET_DIR,'trans-accuracy'),fontsize=15)
#%%
#take latent manifold and tsne_points
z,tsne_pts=plot_tSNE(model=merged_encoder, data=dev_list)
#unique, counts = np.unique(y_dev_cna, return_counts=True)
#print (np.asarray((unique, counts)).T)

from sklearn.cluster import DBSCAN
db = DBSCAN(eps=0.3, min_samples=20).fit(tsne_pts)
core_samples = db.core_sample_indices_
labels = db.labels_
n_clusters = len(set(labels)) - (1 if -1 in labels else 0)
print(n_clusters)
#%%get the clusters DBSCAN
clusters = [tsne_pts[labels == i] for i in range(n_clusters)]
patient_list = np.array(list(range(1000)))
patients = [patient_list[labels == i] for i in range(n_clusters)]

cnaDBSCAN = [dev_list[0][patients[i]] for i in range(n_clusters)]
dnaDBSCAN = [dev_list[1][patients[i]] for i in range(n_clusters)]

zspace = [z[patients[i]] for i in range(n_clusters)]

#histograms of the 
for i in range(n_clusters):
    
    x_df = pd.DataFrame(zspace[i])
    x_bp = x_df.plot.box(return_type='both', patch_artist=True)

#plot distribution of zspace
#then find mean of original zspace
z_center=[np.mean(zspace[i], axis=0) for i in range(n_clusters)]


clusters[0].shape
clusters[1].shape
clusters[2].shape
outliers = tsne_pts[labels == -1]

#%% affinity propagation
print(__doc__)

from sklearn.cluster import AffinityPropagation
from sklearn import metrics
from sklearn.datasets.samples_generator import make_blobs

# #############################################################################
# Generate sample data
X=tsne_pts
labels_true = y_dev_cna.reshape(1000,)
# #############################################################################
# Compute Affinity Propagation
af = AffinityPropagation().fit(X)
cluster_centers_indices = af.cluster_centers_indices_
labels = af.labels_

n_clusters_ = len(cluster_centers_indices)

print('Estimated number of clusters: %d' % n_clusters_)
print("Homogeneity: %0.3f" % metrics.homogeneity_score(labels_true, labels))
print("Completeness: %0.3f" % metrics.completeness_score(labels_true, labels))
print("V-measure: %0.3f" % metrics.v_measure_score(labels_true, labels))
print("Adjusted Rand Index: %0.3f"
      % metrics.adjusted_rand_score(labels_true, labels))
print("Adjusted Mutual Information: %0.3f"
      % metrics.adjusted_mutual_info_score(labels_true, labels))
print("Silhouette Coefficient: %0.3f"
      % metrics.silhouette_score(X, labels, metric='sqeuclidean'))

# #############################################################################
# Plot result
import matplotlib.pyplot as plt
from itertools import cycle

plt.close('all')
plt.figure(1)
plt.clf()

colors = cycle('bgrcmykbgrcmykbgrcmykbgrcmyk')
ClustCenter = [X[cluster_centers_indices[k]] for k in range(n_clusters_)]

result = []
for i in range(n_clusters):
    temp=[a for a in patients[i] if a in cluster_centers_indices]
    result.append(temp)

cnaClust = [dev_list[0][result[i]] for i in range(n_clusters)]
dnaClust = [dev_list[1][result[i]] for i in range(n_clusters)]

for k, col in zip(range(n_clusters_), colors):
    class_members = labels == k
    cluster_center = X[cluster_centers_indices[k]]
    plt.plot(X[class_members, 0], X[class_members, 1], col + '.')
    plt.plot(cluster_center[0], cluster_center[1], 'o', markerfacecolor=col,
             markeredgecolor='k', markersize=14)
    for x in X[class_members]:
        plt.plot([cluster_center[0], x[0]], [cluster_center[1], x[1]], col)

plt.title('Estimated number of clusters: %d' % n_clusters_)
#plt.show()
plt.savefig(os.path.join(NET_DIR,'trans-AP'))

#%%
import string
for i in range(3):
    x_df = pd.DataFrame(cnaClust[i],columns=list(string.ascii_uppercase[0:10]))
    x_bp = x_df.plot.box(return_type='both', patch_artist=True, title="hi")
    plt.title("CNA - Patient Group %s" %i, fontsize=20)
    plt.tick_params(axis='both', which='major', labelsize=15)
    plt.savefig(os.path.join(NET_DIR,'trans-histCNA%d' %i))
    
#%%
for i in range(3):
    x_df = pd.DataFrame(dnaClust[i],columns=list(string.ascii_uppercase[0:10]))
    x_bp = x_df.plot.box(return_type='both', patch_artist=True)
    plt.title("GE - Patient Group %s" %i, fontsize=20)
    plt.tick_params(axis='both', which='major', labelsize=15)
    plt.savefig(os.path.join(NET_DIR,'trans-histDNA%d' %i))


#%%
from sklearn.preprocessing import MinMaxScaler 
z_center = np.asarray(z_center)
#z_scalar = MinMaxScaler()
#z_center_scaled = z_scalar.fit_transform(z_center)
rep_patient=generator.predict(z_center)
rep_patient=np.asarray(rep_patient)
#%% generated patient group mean - CNA
i=0
import numpy as np
import matplotlib.pyplot as plt
x=list(range(0,10))
fig = plt.figure()
ax1 = fig.add_subplot(111)
pg0=ax1.scatter(x,rep_patient[i][0], c="r", label="0")
pg1=ax1.scatter(x,rep_patient[i][1], c="k", label="1")
pg2=ax1.scatter(x,rep_patient[i][2], c="c", label="2")
letters = list(string.ascii_uppercase[0:10])
plt.xticks(x, letters)
plt.title("GE Patterns - Scaled", fontsize=20)
plt.xlabel("Genes", fontsize=15)
plt.ylabel("Mean Value", fontsize=15)
plt.tight_layout()
plt.tick_params(axis='both', which='major', labelsize=15)
plt.legend((pg0,pg1,pg2),
           ('0', '1', '2'),
           scatterpoints=1,
           loc='upper right',
           ncol=3,
           fontsize=8)


#plt.show()
plt.savefig(os.path.join(NET_DIR,'trans-genMEAN-CNA'), bbox_inches="tight")
#%% generated patient group mean - DNA
i=1
import numpy as np
import matplotlib.pyplot as plt
x=list(range(0,10))
fig = plt.figure()
ax1 = fig.add_subplot(111)
pg0=ax1.scatter(x,rep_patient[i][0], c="r", label="0")
pg1=ax1.scatter(x,rep_patient[i][1], c="k", label="1")
pg2=ax1.scatter(x,rep_patient[i][2], c="c", label="2")
letters = list(string.ascii_uppercase[0:10])
plt.xticks(x, letters)
plt.title("CNA Patterns - Scaled", fontsize=20)
plt.xlabel("Genes", fontsize=15)
plt.ylabel("Mean Value", fontsize=15)
plt.tight_layout()
plt.tick_params(axis='both', which='major', labelsize=15)
plt.legend((pg0,pg1,pg2),
           ('0', '1', '2'),
           scatterpoints=1,
           loc='upper right',
           ncol=3,
           fontsize=8)

#plt.show()
plt.savefig(os.path.join(NET_DIR,'trans-genMEAN-DNA'), bbox_inches="tight")

#%% generated patient group mean - CNA
i=0
import numpy as np
import matplotlib.pyplot as plt
x=list(range(0,10))
fig = plt.figure()
ax1 = fig.add_subplot(111)
#dev_scaler.inverse_transform(rep_patient[i][0].reshape(1,10))
pg0=ax1.scatter(x,dev_scaler.inverse_transform(rep_patient[i][0].reshape(1,10)), c="r", label="0")
pg1=ax1.scatter(x,dev_scaler.inverse_transform(rep_patient[i][1].reshape(1,10)), c="k", label="1")
pg2=ax1.scatter(x,dev_scaler.inverse_transform(rep_patient[i][2].reshape(1,10)), c="c", label="2")
letters = list(string.ascii_uppercase[0:10])
plt.xticks(x, letters)
plt.title("CNA Patterns - Unscaled", fontsize=20)
plt.xlabel("Genes", fontsize=15)
plt.ylabel("Mean Value", fontsize=15)

plt.legend((pg0,pg1,pg2),
           ('0', '1', '2'),
           scatterpoints=1,
           loc='upper right',
           ncol=3,
           fontsize=8)


#plt.show()
plt.savefig(os.path.join(NET_DIR,'trans-unscaledgenMEAN-CNA'), bbox_inches="tight")
#%% generated patient group mean - DNA
i=1
import numpy as np
import matplotlib.pyplot as plt
x=list(range(0,10))
fig = plt.figure()
ax1 = fig.add_subplot(111)
pg0=ax1.scatter(x,dev_scaler.inverse_transform(rep_patient[i][0].reshape(1,10)), c="r", label="0")
pg1=ax1.scatter(x,dev_scaler.inverse_transform(rep_patient[i][1].reshape(1,10)), c="k", label="1")
pg2=ax1.scatter(x,dev_scaler.inverse_transform(rep_patient[i][2].reshape(1,10)), c="c", label="2")
letters = list(string.ascii_uppercase[0:10])
plt.xticks(x, letters)
plt.title("GE Patterns - Unscaled",fontsize=20)
plt.xlabel("Genes", fontsize=15)
plt.ylabel("Mean Value", fontsize=15)
plt.tick_params(axis='both', which='major', labelsize=15)
plt.legend((pg0,pg1,pg2),
           ('0', '1', '2'),
           scatterpoints=1,
           loc='upper right',
           ncol=3,
           fontsize=8)


#plt.show()
plt.savefig(os.path.join(NET_DIR,'trans-unscaledgenMEAN-DNA'), bbox_inches="tight")
