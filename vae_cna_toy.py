# -*- coding: utf-8 -*-
"""
Created on Thu Jun 14 23:56:25 2018

@author: Ifrah
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from keras.layers import Lambda, Input, Dense, BatchNormalization
from keras.callbacks import TensorBoard
from keras.models import Model
from keras.utils import plot_model

from params import input_shape, intermediate_dim, latent_dim, original_dim
from params import y_dev_cna, epochs
from params import x_train_cna, y_train_cna, x_dev_cna, batch_size
from vae_functions import sampling, vae_objective

import matplotlib.pyplot as plt
import pandas as pd
import os

#%%

PATH = os.getcwd()
NET_DIR = PATH + "\\vae\\cna\\toy"
LOG_DIR = NET_DIR + '\\tflogs'

#%%
#patientkey=pd.Series(list(range(0, 1000)))
#severity = pd.Series([int(i) for i in y_dev_cna.ravel()])
#df_ = pd.DataFrame({"patient": patientkey,"severity":severity})
#metadata = os.path.join(LOG_DIR, 'metadata.tsv')
##add as many labels to the embedding as you would like.
##this is a label used to identify key points about the latent space
##that is being used to describe the patient
#df_.to_csv(metadata, sep='\t',encoding='utf-8', index = False)
#
weights_file = os.path.join(LOG_DIR, 
               "_vae_dna_%d_latent_%d_epochs.hdf5") % (latent_dim,epochs)
#%%
# VAE model = encoder + decoder

#%% build encoder 
x = Input(shape=input_shape, name='encoder_input')
encoder_x = Dense(intermediate_dim, activation='relu',name="encoder_x")(x)

z_mean = Dense(latent_dim, name='z_mean')(encoder_x)
z_log_var = Dense(latent_dim, name='z_log_var')(encoder_x)

z = Lambda(sampling, name='z')([z_mean, z_log_var])

#%% build decoder 

decoder = Dense(intermediate_dim, activation='relu',name="decoder")
decoder_output = Dense(original_dim, activation='sigmoid', name='decoder_output')

z=BatchNormalization()(z)
z_decoded = decoder(z)
z_decoded=BatchNormalization()(z_decoded)
x_decoded = decoder_output(z_decoded)

#%% instantiate VAE model

vae = Model(inputs=x, outputs=x_decoded, name='vae_mlp')
plot_model(vae,to_file=os.path.join(NET_DIR,'vae_cna.png'), 
           show_shapes=True)
#%%
merged_encoder = Model(inputs=x, outputs=z)

#%%generator
z_input = Input(shape=(latent_dim,))

decoder_h1 = Dense(intermediate_dim, activation='relu')
decoder_h2 = Dense(original_dim, activation='sigmoid',name='decoder_dna_h2')

z_decoded = decoder_h1(z_input)
x_decoded = decoder_h2(z_decoded)

generator = Model(inputs=z_input, outputs=x_decoded)
#%% compile VAE model

vae_loss=vae_objective(z_mean, z_log_var)
vae.compile(optimizer="adam", loss=vae_loss)

#%%
#tb_callback=TensorBoard(log_dir=LOG_DIR, 
#                        batch_size=batch_size,
#                        #save layer output at x epoch 
#                        embeddings_freq=1,
#                        embeddings_metadata='metadata.tsv',
#                        embeddings_data=x_dev_cna,
#                        embeddings_layer_names=['z'],
#                        histogram_freq=1,
#                        write_grads=True)

#%%

from keras.callbacks import History
hist_cb = History()
vae.fit(x_train_cna, x_train_cna, shuffle=True, epochs=epochs, batch_size=batch_size,
        callbacks=[hist_cb], validation_data=(x_dev_cna, x_dev_cna))
vae.save_weights(weights_file)

#%%

#encoder = Model(input=vae.input, output=vae.get_layer(index=-5).output)
#
#decoder_input = Input(shape=(latent_dim,))
#h_decoded = decoder_h1(decoder_input)
#x_decoded = decoder_h2(h_decoded)
#generator = Model(input=decoder_input, output=x_decoded)

#%%

#tensorboard --host localhost --logdir=E:\\Workspace\\MPhil_workspace\\ICM_ML\\vae\\cna\\toy
