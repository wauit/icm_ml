# -*- coding: utf-8 -*-
"""
Created on Wed Jun 20 11:34:02 2018

@author: Ifrah
"""

#%%
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from keras.layers import Lambda, Input, Dense
from keras.models import Model
#from keras.datasets import mnist
from keras.losses import mse, binary_crossentropy, mean_squared_error
from keras.utils import plot_model
from keras import backend as K
from keras import metrics

import tensorflow as tf

from keras.callbacks import TensorBoard
from keras.callbacks import LambdaCallback
import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
import matplotlib.cm as cm
from mpl_toolkits.axes_grid1 import make_axes_locatable

import string 
import argparse
import os

from ggplot import *
from sklearn.manifold import TSNE

np.random.seed(42)

#%% CNA CLASS
class CNA:
    "This class creates and modifies the CNA dataset"
    def __init__(self):
        self.geneNum = 10
        self.data = np.array([]).reshape(0,self.geneNum)
        self.label = np.array([]).reshape(0,1)
        self.severity = np.array([]).reshape(0,1)
        self.cnalist = [-2, -1, 0, 1, 2]
        #self.cnalist = [1, 2, 3, 4, 5]
    def __bioMarkers(self,os):
        if(os>=0 and os<20):
            FAB = np.random.choice(self.cnalist,3, 
                                   p=[0.05,0.05,0.1,0.4,0.4])
            DIE = np.random.choice(self.cnalist,3, 
                                   p=[0.4,0.4,0.1,0.05,0.05])
            CGHJ = np.random.choice(self.cnalist,4, 
                                   p=[0.225,0.225,0.1,0.225,0.225])
            severity = 0
            
        elif(os>=20 and os<40):
            FAB = np.random.choice(self.cnalist,3, 
                                   p=[0.025,0.45,0.05,0.45,0.025])
            DIE = np.random.choice(self.cnalist,3, 
                                   p=[0.05,0.4,0.1,0.4,0.05])
            CGHJ = np.random.choice(self.cnalist,4, 
                                   p=[0.225,0.225,0.1,0.225,0.225])
            severity = 1
            
        elif(os>=40):
            FAB = np.random.choice(self.cnalist,3, 
                                   p=[0.05,0.4,0.1,0.4,0.05])
            DIE = np.random.choice(self.cnalist,3, 
                                   p=[0.05,0.05,0.1,0.4,0.4])
            CGHJ = np.random.choice(self.cnalist,4, 
                                   p=[0.225,0.225,0.1,0.225,0.225])
            severity = 2
            
        CNApattern = np.array([FAB[1],FAB[2],CGHJ[0],DIE[0],DIE[2],FAB[0],CGHJ[1],CGHJ[2],DIE[1],CGHJ[3]]).reshape(1,10)
        return(CNApattern, severity) 
            
    def addPatient(self, os):
        d,s = self.__bioMarkers(os)
        self.data = np.vstack((self.data, d))
        self.labels = np.vstack((self.label, os))
        self.severity = np.vstack((self.severity,s))

#%% TRAIN SET
x = CNA()
for i in range(200):
    x.addPatient(0)
    x.addPatient(5)
    x.addPatient(10)
    x.addPatient(15)
    x.addPatient(20)
    x.addPatient(20)
    x.addPatient(25)
    x.addPatient(30)
    x.addPatient(35)
    x.addPatient(40)

#showPatients(x.data[1:10,1:10])    

# x.data
# x.labels    
    
#%% TEST SET
y=CNA()
for i in range(100):
    y.addPatient(0)
    y.addPatient(5)
    y.addPatient(10)
    y.addPatient(25)
    y.addPatient(26)
    y.addPatient(30)
    y.addPatient(35)
    y.addPatient(41)
    y.addPatient(45)
    y.addPatient(48)
    
#%% PARAMETERS

# MNIST dataset
x_train = x.data
y_train = x.severity
x_test = y.data
y_test = y.severity


image_size = x_train.shape[1]
original_dim = x.geneNum

# network parameters
input_shape = (original_dim,) #(original_dim, )
intermediate_dim = 5
batch_size = 5
latent_dim = 5
epochs = 2
noise_std = .01      
#%% PLOTTING FUNCTIONS
def showPatients(data_matrix):
    fig = plt.figure(figsize=(12, 10)) 
    ax = fig.add_subplot(111)
    
    colormap = cm.ScalarMappable(cmap="jet")
    img = colormap.to_rgba(data_matrix)
    img = np.repeat(img, 5, axis=1)
    ax.imshow(img,interpolation='nearest', vmin=0.5, vmax=0.99)
    
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)
    colormap.set_array(data_matrix)
    fig.colorbar(colormap, cax=cax)

    fig.show()

def plot_first2D(models,
                 data,
                 batch_size=10,
                 model_name="vae_CNA"):
    """Plots labels and MNIST digits as function of 2-dim latent vector
    # Arguments:
        models (tuple): encoder and decoder models
        data (tuple): test data and label
        batch_size (int): prediction batch size
        model_name (string): which model is using this function
    """

    encoder, decoder = models
    x_test, y_test = data
    os.makedirs(model_name, exist_ok=True)

    filename = os.path.join(model_name, "vae_mean.png")
    # display a 2D plot of the digit classes in the latent space
    z_mean, _, _ = encoder.predict(x_test,batch_size=batch_size)
    fig = plt.figure(figsize=(12, 10)) 
    ax = fig.add_subplot(111)
    m = cm.ScalarMappable(cmap=cm.jet)
    m.set_array(y_test)
    #.ravel() fixed the dimension/shape issue
    ax.scatter(x=z_mean[:, 0],y= z_mean[:, 1], c=y_test.ravel(),cmap=cm.jet)
    plt.colorbar(m)
    plt.xlabel("z[0]")
    plt.ylabel("z[1]")
    plt.savefig(filename)
    plt.show()

def plot_tSNE(models,
                 data,
                 perplexity = 30,
                 batch_size=batch_size,
                 model_name="vae_CNA"):
    """Plots labels and MNIST digits as function of 2-dim latent vector
    # Arguments:
        models (tuple): encoder and decoder models
        data (tuple): test data and label
        batch_size (int): prediction batch size
        model_name (string): which model is using this function
    """
    encoder, generator = models
    x_test, y_test = data
    
    z = encoder.predict(x_test,verbose=1,
                        batch_size=batch_size)

    tsne = TSNE(n_components=2,perplexity = perplexity)
    X_tsne = tsne.fit_transform(z)
    
    fig = plt.figure(figsize=(12, 10)) 
    ax = fig.add_subplot(111)
    
    m = cm.ScalarMappable(cmap=cm.jet)
    m.set_array(y_test)
    ax.scatter(X_tsne[:, 0], X_tsne[:, 1], c=y_test.ravel(),cmap=cm.jet)
    plt.colorbar(m)
    plt.show()    
    

    
#%% PLOT HISTOGRAMS OF CNA PATTERNS, BY SEVERITY
x_df = pd.DataFrame(x.data,columns=list(string.ascii_uppercase[0:10]))
x_bp = x_df.plot.box(return_type='both', patch_artist=True)

x_df_low = x_df[x.severity==0]
x_bp_low = x_df_low.plot.box(return_type='both', patch_artist=True, color="red")
x_df_low


x_df_med = x_df[x.severity==1]
x_bp_med = x_df_med.plot.box(return_type='both', patch_artist=True, color="gold")
x_df_med

x_df_high = x_df[x.severity==2]
x_bp_high = x_df_high.plot.box(return_type='both', patch_artist=True, color="green")
x_df_high


#%%
PATH = os.getcwd()
LOG_DIR = PATH + '\\vae\\dnaEXPR\\demo'


#%%SAMPLING FROM LATENT SPACE
# reparameterization trick
# instead of sampling from Q(z|X), sample eps = N(0,I)
# z = z_mean + sqrt(var)*eps
def sampling(args):
    """Reparameterization trick by sampling from an isotropic unit Gaussian.
    # Arguments:
        args (tensor): mean and log of variance of Q(z|X)
    # Returns:
        z (tensor): sampled latent vector
    """

    z_mean, z_log_var= args
    z = K.random_normal(shape=(batch_size, latent_dim),
                                    mean=0., stddev=noise_std)
    z *= K.exp(.5 * z_log_var)
    z += z_mean
    # by default, random_normal has mean=0 and std=1.0
    return(z)

#%%
# VAE model = encoder + decoder

#%%

# build encoder model
inputs = Input(shape=input_shape, name='encoder_input')
encoder_h = Dense(intermediate_dim, activation='relu')(inputs)
#this has no activation. it just takes h, does nothing to it
#and the output space has a dimensionality of latent_dim

#feed same inputs to these nodes. these will later be used to calculate 
#the z
z_mean = Dense(latent_dim, name='z_mean')(encoder_h)
z_log_var = Dense(latent_dim, name='z_log_var')(encoder_h)

# use reparameterization trick to push the sampling out as input
# note that "output_shape" isn't necessary with the TensorFlow backend
z = Lambda(sampling, name='z')([z_mean, z_log_var])

# instantiate encoder model
#encoder = Model(inputs=inputs, outputs=[z_mean, z_log_var, z], name='encoder')
#encoder.summary()
#plot_model(encoder, to_file='vae_mlp_encoder.png', show_shapes=True)

#%%
# build decoder model
decoder_h1 = Dense(intermediate_dim, activation='relu')
decoder_h2 = Dense(original_dim, activation='sigmoid',name='decoder_h2')
z_decoded = decoder_h1(z)
x_decoded = decoder_h2(z_decoded)
# instantiate decoder model
#decoder = Model(latent_inputs, x_decoded, name='decoder')
#decoder.summary()
#plot_model(decoder, to_file='vae_mlp_decoder.png', show_shapes=True)

#%%
# instantiate VAE model
#outputs = decoder(encoder(inputs)[2])
vae = Model(inputs=inputs, outputs=x_decoded, name='vae_mlp')
plot_model(vae,to_file=os.path.join(LOG_DIR, 'vae_network.png'),
           show_shapes=True)

#%%
# Objective function minimized by autoencoder
def vae_objective(x, x_decoded):
    beta=0.5
    loss = mean_squared_error(x, x_decoded)
    #regularizer. this is the KL of q(z|x) given that the 
    #distribution is N(0,1) (or any known distribution)
    kl_regu = - beta * K.mean(1 + z_log_var - K.square(z_mean) - 
                              K.exp(z_log_var), axis=-1)
    return(loss + kl_regu)
    
#%%
vae.compile(optimizer="adam", loss=vae_objective)
#%%


#%%

metadata = os.path.join(LOG_DIR, 'metadata.tsv')

with open(metadata, 'w') as metadata_file:
    for label in y_test:
        metadata_file.write('%d\t%d\n' % (label,label))

tb_callback=TensorBoard(log_dir=LOG_DIR, 
                        batch_size=batch_size,
                        #save layer output at x epoch 
                        embeddings_freq=1,
                        embeddings_metadata='metadata.tsv',
                        embeddings_data=x_test,
                        embeddings_layer_names=['z'])

weights_file = os.path.join(LOG_DIR, 
               "_vae_dna_%d_latent_%d_epochs.hdf5") % (latent_dim,epochs)
if os.path.isfile(weights_file):
    vae.load_weights(weights_file)
else:
    from keras.callbacks import History
    hist_cb = History()
    vae.fit(x_train, x_train, shuffle=True, epochs=epochs, batch_size=batch_size,
            callbacks=[hist_cb, tb_callback], validation_data=(x_test, x_test))
    vae.save_weights(weights_file)
    #hist_cb.history.keys()
    # plot convergence curves to show off
fig_loss=plt.figure()
ax2=fig_loss.add_subplot(111)
ax2.plot(hist_cb.history["loss"], label="training")
ax2.plot(hist_cb.history["val_loss"], label="validation")
plt.grid("on")
plt.xlabel("epoch")
plt.ylabel("loss")
plt.legend(loc="best")
#%%
#for layer in vae.layers:
#    print(layer.name,layer.trainable)
#    print('Layer Configuration:')
#    print(layer.get_config(),
#          end='\n{}\n'.format('----'*10))
#vae.layers[4].name
#vae.layers[4].get_config()
#z_mean=vae.layers[4].output
#%%

#%%
encoder = Model(inputs=vae.input, outputs=z_mean)
#encoder = Model(input=inputs, outputs=z)
#%%
decoder_input = Input(shape=(latent_dim,))
h_decoded = decoder_h1(decoder_input)
x_decoded = decoder_h2(h_decoded)
generator = Model(input=decoder_input, output=x_decoded)

#%%
#models = (encoder, generator)
#data = (x_test, y_test)
#
##plot_first2D(models,
##             data,
##             batch_size=batch_size,
##             model_name="vae_first2D")
#    
#p1=plot_tSNE(models,
#             data,
#             batch_size=batch_size,
#             model_name="vae_tSNE", perplexity=5)

#%%
#z=encoder.predict(x_test)
#
#z_mu=np.mean(z, axis=0)
#z_var=np.std(z, axis=0)
#
#def KL_elem(mu,sigma):
#    kl=-0.5*( 1 + np.log(sigma) - np.square(mu) - sigma )
#    return(np.log(kl))
#KL_elem(z_mu,z_var)


#%%
#from ipywidgets import FloatSlider, interact
#
##we will sample points within given standard deviations
#humour = FloatSlider(min=-15, max=15, step=3, value=0)
#pose = FloatSlider(min=-15, max=15, step=3, value=0)
#
#@interact(pose=pose, humour=humour)
#def do_thumb(humour, pose):
#    z_sample = np.array([[humour, pose]]) * noise_std
#    x_decoded = generator.predict(z_sample)
#    face = x_decoded[0].reshape(1,10)
#    
#    colormap = cm.ScalarMappable(cmap="jet")
#    img = colormap.to_rgba(face)
#    img = np.repeat(img, 5, axis=1)
#    
#    plt.figure(figsize=(11.5, 11.5))
#    ax = plt.subplot(111)
#    ax.imshow(img)
#    plt.axis("off")
    
#%%
#PATH = os.getcwd()
#LOG_DIR = PATH + '\\tflogs\\'
#
#metadata = os.path.join(LOG_DIR, 'metadata.tsv')
#
#with open(metadata, 'w') as metadata_file:
#    for label in x.severity:
#        metadata_file.write('%d\n' % label)
#
#tb_callback=TensorBoard(log_dir=os.path.join(LOG_DIR,'VAE'), 
#                        embeddings_metadata='metadata.tsv',
#                        embeddings_layer_names=['embeddings_names'])

#%%   


#%%
#    plot_first2D(models,
#                 data,
#                 batch_size=batch_size,
#                 model_name="vae_mlp")
    
#p1=plot_tSNE(models,
#             data,
#             batch_size=batch_size,
#             model_name="vae_mlp", perplexity=5)
#p2=plot_tSNE(models,
#             data,
#             batch_size=batch_size,
#             model_name="vae_mlp", perplexity=30)
#p3=plot_tSNE(models,
#             data,
#             batch_size=batch_size,
#             model_name="vae_mlp", perplexity=50)
#p4=plot_tSNE(models,
#             data,
#             batch_size=batch_size,
#             model_name="vae_mlp", perplexity=100)
#p45=plot_tSNE(models,
#             data,
#             batch_size=100,
#             model_name="vae_mlp", perplexity=100)

#%%
#showPatients(x.data[1:10])
#%%
