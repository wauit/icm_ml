# -*- coding: utf-8 -*-
"""
Created on Fri Jun 29 12:05:12 2018

@author: Ifrah
"""

#%%
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from keras.layers import Lambda, Input, Dense, BatchNormalization
from keras.callbacks import TensorBoard
from keras.models import Model
from keras.utils import plot_model

from params import input_shape, intermediate_dim, latent_dim
from params import y_dev_cna, epochs
from params import x_train_st, y_train_cna, x_dev_st, batch_size
from vae_functions import sampling, vae_objective

from plotting import plot_tSNE

import tensorflow as tf
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os

#%%
PATH = os.getcwd()
NET_DIR = PATH + "\\vae\\concatenate\\toy"
LOG_DIR = NET_DIR + '\\tflogs'

#%%
patientkey=pd.Series(list(range(0, 1000)))
severity = pd.Series([int(i) for i in y_dev_cna.ravel()])
df_ = pd.DataFrame({"patient": patientkey,"severity":severity})
metadata = os.path.join(LOG_DIR, 'metadata.tsv')
#add as many labels to the embedding as you would like.
#this is a label used to identify key points about the latent space
#that is being used to describe the patient
df_.to_csv(metadata, sep='\t',encoding='utf-8', index = False)

weights_file = os.path.join(LOG_DIR, 
               "_vae_dna_%d_latent_%d_epochs.hdf5") % (latent_dim,epochs)

#%% build encoder 
original_dim=x_train_st.shape[1]
concat_inputs = Input(shape=(original_dim,), name='concat_input')

encoder = Dense(intermediate_dim,activation='relu')(concat_inputs)
z_mean = Dense(latent_dim, name='z_mean')(encoder)
z_log_var = Dense(latent_dim, name='z_log_var')(encoder)
z = Lambda(sampling, name='z')([z_mean, z_log_var])

decoder_h1 = Dense(intermediate_dim, activation='relu')
decoder_h2 = Dense(original_dim, activation='sigmoid',name='decoder_h2')
                       
z_decoded = decoder_h1(z)
x_decoded = decoder_h2(z_decoded)

#%% instantiate VAE model

vae = Model(inputs=concat_inputs, outputs=x_decoded)
plot_model(vae,to_file=os.path.join(NET_DIR,'vae_concatenated.png'), 
           show_shapes=True)

#%%encoder
merged_encoder = Model(inputs=concat_inputs, outputs=z)

#%%
z_input = Input(shape=(latent_dim,))

decoder_h1 = Dense(intermediate_dim, activation='relu')
decoder_h2 = Dense(original_dim, activation='sigmoid',name='decoder_h2')

z_decoded_dna = decoder_h1(z_input)
x_decoded_dna = decoder_h2(z_decoded_dna)

generator = Model(inputs=z_input, outputs=x_decoded_dna)
#%% compile VAE model

vae_loss=vae_objective(z_mean, z_log_var)
vae.compile(optimizer="adam", loss=vae_loss)

#%%
embed_freq=1
tb_callback=TensorBoard(log_dir=LOG_DIR, 
                        batch_size=batch_size,
                        #save layer output at x epoch 
                        embeddings_freq=embed_freq,
                        embeddings_metadata='metadata.tsv',
                        embeddings_data=x_dev_st,
                        embeddings_layer_names=['z'],
                        histogram_freq=1,
                        write_grads=True)
#%%
from keras.callbacks import History
hist_cb = History()
vae.fit(x_train_st, x_train_st, shuffle=True, epochs=epochs, batch_size=batch_size,
        callbacks=[hist_cb], validation_data=(x_dev_st, x_dev_st))
vae.save_weights(weights_file)

#tensorboard --host localhost --logdir=E:\\Workspace\\MPhil_workspace\\ICM_ML\\vae\\concatenate\\toy\\tflogs
