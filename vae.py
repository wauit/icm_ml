# -*- coding: utf-8 -*-
"""
Created on Thu Jun 14 23:56:25 2018

@author: Ifrah
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from keras.layers import Lambda, Input, Dense, BatchNormalization
from keras.models import Model
from keras.utils import plot_model


from params import input_shape, intermediate_dim, latent_dim, original_dim
from params import y_dev, epochs
from vae_functions import sampling, vae_objective

import os


#%%

PATH = os.getcwd()
NET_DIR = PATH + "\\vae_bn"
LOG_DIR = NET_DIR + '\\tflogs'

#%%

metadata = os.path.join(NET_DIR, 'metadata.tsv')

with open(metadata, 'w') as metadata_file:
    for label in y_dev:
        metadata_file.write('%d\n' % (label))

weights_file = os.path.join(LOG_DIR, 
               "_vae_bn_%d_latent_%d_epochs.hdf5") % (latent_dim,epochs)
#%%
# VAE model = encoder + decoder

#%% build encoder 
x = Input(shape=input_shape, name='encoder_input')
encoder_x = Dense(intermediate_dim, activation='relu',name="encoder_x")(x)

z_mean = Dense(latent_dim, name='z_mean')(encoder_x)
z_log_var = Dense(latent_dim, name='z_log_var')(encoder_x)

z = Lambda(sampling, name='z')([z_mean, z_log_var])

#%% build decoder 

decoder = Dense(intermediate_dim, activation='relu',name="decoder")
decoder_output = Dense(original_dim, activation='sigmoid', name='decoder_output')

#z=BatchNormalization()(z1)
z_decoded = decoder(z)
#z_decoded_N=BatchNormalization()(z_decoded)
x_decoded = decoder_output(z_decoded)

#%% instantiate VAE model

vae = Model(inputs=x, outputs=x_decoded, name='vae_mlp')
plot_model(vae,to_file=os.path.join(NET_DIR,'vae_bn_network.png'), 
           show_shapes=True)

#%% compile VAE model

vae_loss=vae_objective(z_mean, z_log_var)
vae.compile(optimizer="adam", loss=vae_loss)

#%%
