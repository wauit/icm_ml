# -*- coding: utf-8 -*-
"""
Created on Sat Jun 30 23:46:30 2018

@author: Ifrah
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import os

from params import batch_size
from ggplot import *
from sklearn.manifold import TSNE

from vae_cna_toy import *
from keras.models import Model

import matplotlib.pyplot as plt
import matplotlib.cm as cm
from mpl_toolkits.axes_grid1 import make_axes_locatable
from sklearn.preprocessing import StandardScaler

import numpy as np

#%%
def plot_tSNE(model,
                 data,
                 perplexity = 30,
                 batch_size=batch_size,
                 model_name="vae_CNA"):
    """Plots labels and MNIST digits as function of 2-dim latent vector
    # Arguments:
        models (tuple): encoder and decoder models
        data (tuple): test data and label
        batch_size (int): prediction batch size
        model_name (string): which model is using this function
    """
    encoder = model
    x_dev = data
    
    z = encoder.predict(x_dev,verbose=1,batch_size=batch_size)

    tsne = TSNE(n_components=2,perplexity = 50)
    X_tsne = tsne.fit_transform(z)
    #X = X_tsne
    X = StandardScaler().fit_transform(X_tsne) 
    fig = plt.figure(figsize=(12, 10)) 
    ax = fig.add_subplot(111)
    m = cm.ScalarMappable(cmap=cm.jet)
    m.set_array(y_dev_cna)
    ax.scatter(X[:,0],X[:,1], c=y_dev_cna.ravel(),cmap=cm.jet)
    plt.colorbar(m)
    plt.show() 
    
    return(X)
#%%
merged_encoder=Model(inputs=x, outputs=z)
z=merged_encoder.predict(x_dev_cna,verbose=1,batch_size=batch_size)

#%%
tsne_pts=plot_tSNE(model=merged_encoder, data=x_dev_cna)
unique, counts = np.unique(y_dev_cna, return_counts=True)
print (np.asarray((unique, counts)).T)

from sklearn.cluster import DBSCAN
db = DBSCAN(eps=0.3, min_samples=20).fit(tsne_pts)
core_samples = db.core_sample_indices_
labels = db.labels_
n_clusters = len(set(labels)) - (1 if -1 in labels else 0)
print(n_clusters)

clusters = [tsne_pts[labels == i] for i in range(n_clusters)]
patient_list = np.array(list(range(1000)))
patients = [patient_list[labels == i] for i in range(n_clusters)]

cnaDBSCAN = [x_dev_cna[0][patients[i]] for i in range(n_clusters)]
dnaDBSCAN = [x_dev_cna[1][patients[i]] for i in range(n_clusters)]

#cnaClust[0].


clusters[0].shape
clusters[1].shape
clusters[2].shape
outliers = tsne_pts[labels == -1]


#%%
#data = tsne_pts
#labels = ['point{0}'.format(i) for i in range(data.shape[0])]
#
#plt.subplots_adjust(bottom = 0.1)
#plt.scatter(data[:,0],data[:,1], c=y_dev_cna.ravel(),cmap=cm.jet)
#
#for label, x, y in zip(labels, data[:, 0], data[:, 1]):
#    plt.annotate(
#        label,
#        xy=(x, y), xytext=(-20, 20),
#        textcoords='offset points', ha='right', va='bottom',
#        bbox=dict(boxstyle='round,pad=0.5', fc='yellow', alpha=0.5),
#        arrowprops=dict(arrowstyle = '->', connectionstyle='arc3,rad=0'))
#
#plt.show()

#%% affinity propagation
print(__doc__)

from sklearn.cluster import AffinityPropagation
from sklearn import metrics
from sklearn.datasets.samples_generator import make_blobs

# #############################################################################
# Generate sample data
X=tsne_pts
labels_true = y_dev_cna.reshape(1000,)
# #############################################################################
# Compute Affinity Propagation
af = AffinityPropagation().fit(X)
cluster_centers_indices = af.cluster_centers_indices_
labels = af.labels_

n_clusters_ = len(cluster_centers_indices)

print('Estimated number of clusters: %d' % n_clusters_)
print("Homogeneity: %0.3f" % metrics.homogeneity_score(labels_true, labels))
print("Completeness: %0.3f" % metrics.completeness_score(labels_true, labels))
print("V-measure: %0.3f" % metrics.v_measure_score(labels_true, labels))
print("Adjusted Rand Index: %0.3f"
      % metrics.adjusted_rand_score(labels_true, labels))
print("Adjusted Mutual Information: %0.3f"
      % metrics.adjusted_mutual_info_score(labels_true, labels))
print("Silhouette Coefficient: %0.3f"
      % metrics.silhouette_score(X, labels, metric='sqeuclidean'))

# #############################################################################
# Plot result
import matplotlib.pyplot as plt
from itertools import cycle

plt.close('all')
plt.figure(1)
plt.clf()

colors = cycle('bgrcmykbgrcmykbgrcmykbgrcmyk')
ClustCenter = [X[cluster_centers_indices[k]] for k in range(n_clusters_)]

result = []
for i in range(n_clusters):
    temp=[a for a in patients[i] if a in cluster_centers_indices]
    result.append(temp)

cnaClust = [x_dev_cna[0][result[i]] for i in range(n_clusters)]
dnaClust = [x_dev_cna[1][result[i]] for i in range(n_clusters)]



for k, col in zip(range(n_clusters_), colors):
    class_members = labels == k
    cluster_center = X[cluster_centers_indices[k]]
    plt.plot(X[class_members, 0], X[class_members, 1], col + '.')
    plt.plot(cluster_center[0], cluster_center[1], 'o', markerfacecolor=col,
             markeredgecolor='k', markersize=14)
    for x in X[class_members]:
        plt.plot([cluster_center[0], x[0]], [cluster_center[1], x[1]], col)

plt.title('Estimated number of clusters: %d' % n_clusters_)
plt.show()

#%%
import string
for i in range(3):
    x_df = pd.DataFrame(cnaClust[i],columns=list(string.ascii_uppercase[0:10]))
    x_bp = x_df.plot.box(return_type='both', patch_artist=True)

for i in range(3):
    x_df = pd.DataFrame(dnaClust[i],columns=list(string.ascii_uppercase[0:10]))
    x_bp = x_df.plot.box(return_type='both', patch_artist=True)

#%%
#def fit(self):
#    self.graph = tf.Graph()
#    with self.graph.as_default():
#        self.X = tf.placeholder(self.dtype, shape=self.data.shape)
#
#        # Perform SVD
#        singular_values, u, _ = tf.svd(self.X)
#
#        # Create sigma matrix
#        sigma = tf.diag(singular_values)
#
#    with tf.Session(graph=self.graph) as session:
#        self.u, self.singular_values, self.sigma = session.run([u, singular_values, sigma],
#                                                               feed_dict={self.X: self.data})
#        
##%%
#def reduce(self, n_dimensions=None, keep_info=None):
#    if keep_info:
#        # Normalize singular values
#        normalized_singular_values = self.singular_values / sum(self.singular_values)
#        # Create the aggregated ladder of kept information per dimension
#        ladder = np.cumsum(normalized_singular_values)
#        # Get the first index which is above the given information threshold
#        index = next(idx for idx, value in enumerate(ladder) if value >= keep_info) + 1
#        n_dimensions = index
#    with self.graph.as_default():
#        # Cut out the relevant part from sigma
#        sigma = tf.slice(self.sigma, [0, 0], [self.data.shape[1], n_dimensions])
#        # PCA
#        pca = tf.matmul(self.u, sigma)
#    with tf.Session(graph=self.graph) as session:
#        return session.run(pca, feed_dict={self.X: self.data})
#
##%%
#from sklearn import datasets
#import matplotlib.pyplot as plt
#import seaborn as sns
#tf_pca = TF_PCA(iris_dataset.data, iris_dataset.target)
#tf_pca.fit()
#pca = tf_pca.reduce(keep_info=0.9)  # Results in 2 dimensions
#color_mapping = {0: sns.xkcd_rgb['bright purple'], 1: sns.xkcd_rgb['lime'], 2: sns.xkcd_rgb['ochre']}
#colors = list(map(lambda x: color_mapping[x], tf_pca.target))
#plt.scatter(pca[:, 0], pca[:, 1], c=colors)
